package test
import play.api.db._
import play.api.Play.current
import play.api.Play
import play.api.db.slick._
import play.api.db.slick.Config.driver.simple._
import org.specs2.mutable._
import play.api.test._
import play.api.test.Helpers._
import models._
import scala.slick.session.Database.threadLocalSession
import utils.Logger
import mocks.Mocks

class BarSpec extends Specification with Logger with hasDB {

  import models._

  val testBar = Bar(name = "uj kocsma", address = "1119 Budafoki ut 100", verified = false)
  val testHostUser = User(name = "new host", email = "testhost@gmail.com", googleId = "843360392", facebookId = "facebook")

  "Bar model" should {

    "be able to create a new bar" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val bars = Bars.listAll
        val size = bars.size
        Bars.insert(testBar)
        val barsWithNew = Bars.listAll
        barsWithNew.size must equalTo(size + 1)
      }
    }
    
    "be able to register a new bar which means the register user becomes an admin" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val testHostUserId = Guests.insert(testHostUser)

        val bars = Bars.listAll
        val hosts = Hosts.listAll
        val size = bars.size
        val registeredBarId = Bars.register(testBar,testHostUserId)
        
        val barsWithNew = Bars.listAll
        barsWithNew.size must equalTo(size + 1)
        
        val hostsWithNew = Hosts.listAll
        hostsWithNew.size must equalTo(size + 1)
        
        Hosts.listAll.filter(h => (h.barId == registeredBarId && h.userId == testHostUserId)) must not be Nil
      }
    }

    "be updated if needed" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val id = Bars.insert(testBar)
        Bars.update(id, Bar(name = "uj kocsma up", address = "1119 Budafoki ut 222", verified = false))

        val Some(bar) = Bars.findById(id)

        bar.name must equalTo("uj kocsma up")
        bar.address must equalTo("1119 Budafoki ut 222")

      }
    }

    "be deleted if needed" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val id = Bars.insert(testBar)
        Bars.delete(id)

        val deleted = Bars.findById(id)

        deleted must be equalTo None
      }
    }

    "be able to add new table" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val id = Bars.insert(testBar)

        val newName = "random name"
        val idOfTheNewTable = Bars.addTable(newName)(Bars.findByIdOrThrowIfNot(id))

        val Some(newTable) = BarTables.findById(idOfTheNewTable)

        newTable.name must be equalTo newName
      }
    }

    "be able to update a table" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val oldName = "random name"
        val newName = "random name but new"

        val id = Bars.insert(testBar)
        val idOfTheNewTable = BarTables.insert(BarTable(name = oldName, barId = id))

        Bars.updateTable(idOfTheNewTable, newName)(Bars.findByIdOrThrowIfNot(id))
        val Some(newTable) = BarTables.findById(idOfTheNewTable)

        newTable.name must be equalTo newName
      }
    }
    
    
    "be able to delete a table" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val oldName = "random name"

        val id = Bars.insert(testBar)
        val idOfTheNewTable = BarTables.insert(BarTable(name = oldName, barId = id))

        Bars.deleteTable(idOfTheNewTable)(Bars.findByIdOrThrowIfNot(id))
        val deletedTable = BarTables.findById(idOfTheNewTable)

        deletedTable must be equalTo None
      }
    }

    "be able to display a bar with its tables" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val id = Bars.insert(testBar)
        BarTables.insert(BarTable(name = "1", barId = id))
        BarTables.insert(BarTable(name = "2", barId = id))
        val barWithTables = Bars.withTables(id)

        barWithTables match {
          case (bar, tables) =>
            bar.name must be equalTo "uj kocsma"
            tables.size must be equalTo 2
        }
      }
    }

  }
}