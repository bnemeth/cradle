package test

import scala.slick.driver.H2Driver.simple._
import org.specs2.mutable._
import play.api.test._
import play.api.test.Helpers._
import models._
import play.api.db._
import play.api.Play.current
import Database.threadLocalSession
import utils._
import mocks._
import mocks.Mocks._
import org.joda.time.Period
import org.joda.time.DateTime
import org.joda.time.Duration

class OrderSpec extends Specification with hasDB with Logger {

  import models._

  // -- Date helpers

  def dateIs(date: java.util.Date, str: String) = new java.text.SimpleDateFormat("yyyy-MM-dd").format(date) == str

  // --

  "Order model" should {

    "be able to place new orders" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        database.withSession {

          val guestId = Mocks.users("itu").id.get
          val tableId = Seats.findByGuest(guestId).get.tableId

          val activeOrdersCountQ =
            Query((for {
              (guests, orders) <- BarTables.guests(tableId) join Orders on (_.id === _.guestId)
              if (orders.delivered.isNull)
            } yield (orders)).length)

          val activeOrdersList =
            (for {
              (guests, orders) <- BarTables.guests(tableId) join Orders on (_.id === _.guestId)
              if (orders.delivered.isNull)
            } yield (orders))

          val activeOrdersCount = activeOrdersCountQ.first
          val sumBefore = activeOrdersList.list.map(_.sum).sum

          logger.debug("Count of orders before new order: {}", activeOrdersCount)
          logger.debug("Sum of orders before new order: {}", sumBefore)

          Orders.placeOrder(guestId, Basket(Map(MProducts("hamburger").id.get -> 3, MProducts("keseru mez").id.get -> 3, MProducts("csokimousse").id.get -> 1)))
          val sumOfNewOrder = Mocks.sumOfOrder(List(("hamburger", 3), ("keseru mez", 3), ("csokimousse", 1)))
          logger.debug("Sum of new order: {}", sumOfNewOrder)

          val activeOrdersCountAfterInsert = activeOrdersCountQ.first

          val sumAfter = activeOrdersList.list.map(_.sum).sum

          logger.trace("Orders list: {}", activeOrdersList.list)

          logger.trace("Orders list: {}", activeOrdersList.list.map(_.products))

          logger.debug("Count of orders after new order: {}", activeOrdersCountAfterInsert)
          logger.debug("Sum of orders after new order: {}", sumAfter)
          sumAfter must be equalTo (sumBefore + sumOfNewOrder)
          activeOrdersCountAfterInsert must be equalTo (activeOrdersCount + 1)
        }
      }
    }

    "be able to get all active orders on the table by guestId" in
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val orders = Orders.getActiveOrdersOfTableByGuest(1)
        val durationNowAndLatest = new Duration(orders.latestOrderAdded, new DateTime)
        logger.debug("Duration: {}", durationNowAndLatest)
        durationNowAndLatest.getStandardMinutes() must be equalTo (0)
      }

    "be able to get all delivered orders on the table by guestId" in
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val orders = Orders.getDeliveredOrdersOfTableByGuest(1)
        val durationNowAndLatest = new Duration(orders.latestOrderAdded, new DateTime)
        logger.debug("Duration: {}", durationNowAndLatest)
        durationNowAndLatest.getStandardMinutes() must be equalTo (0)
      }

    "be able to get all delivered orders of a guest" in
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val orders = Orders.getDeliveredOrdersOfGuest(1)
        orders.length must beGreaterThan(0)
      }

    "be able to return no delivered orders if a guest has no unpaid orders" in
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val orders = Orders.getDeliveredOrdersOfGuest(2)
        orders.length must beEqualTo(0)
      }
  }
}
