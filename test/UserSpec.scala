package test

import scala.slick.driver.H2Driver.simple._

import org.specs2.mutable._
import play.api.test._
import play.api.test.Helpers._
import models._
import play.api.db._
import play.api.Play.current
import Database.threadLocalSession

class UserSpec extends Specification {

  import models._

  // --

  "User model" should {

    "be retrieved by id" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val Some(user) = Guests.findById(1)
		user.facebookId must not be null
      }
    }

    "be listed " in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val users = Guests.listAll

        users.size must beGreaterThan(2) 
      }
    }

    "a host should have host right" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

       Hosts.isHost(1) must beTrue
      }
    }
    
    "a guest mustn't have host right" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

       Hosts.isHost(2) must beFalse
      }
    }
    
    "be listed with a filter" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val testHostUser = User(name = "new host", email = "testhost@gmail.com", googleId = "843360392", facebookId = "facebook")
        val testHostUserId = Guests.insert(testHostUser)

        val filter = "testhost"
        val users = Guests.listByFilter(filter)

        users.size must be equalTo(1) 
      }
    }
    
    "be listed with an empty filter" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val testHostUser = User(name = "new host", email = "testhost@gmail.com", googleId = "843360392", facebookId = "facebook")
        val testHostUserId = Guests.insert(testHostUser)

        val filter = ""
        val users = Guests.listByFilter(filter)

        users.size must be equalTo(Guests.listAll.size) 
      }
    }

  }

}