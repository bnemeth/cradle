package test
import play.api.db._
import play.api.Play.current
import play.api.Play
import play.api.db.slick._
import play.api.db.slick.Config.driver.simple._
import org.specs2.mutable._
import play.api.test._
import play.api.test.Helpers._
import models._
import scala.slick.session.Database.threadLocalSession
import utils.Logger
import mocks.Mocks

class BarTableSpec extends Specification with Logger with hasDB {

  import models._

  // -- Date helpers

  def dateIs(date: java.util.Date, str: String) = new java.text.SimpleDateFormat("yyyy-MM-dd").format(date) == str
  val hamburgerName = "hamburger"
  val hamburgerPrice = 1200

  // --

  "BarTable model" should {

    "current state of table should contain the name of the table, its guest list, its order list (new, not paid)" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val state = BarTables.getTableStateFull(1)

        state.guests.length must equalTo(2)
        state.barTable.name must equalTo("1-es")

        val activeOrders = state.ordersGroupedByStateWithOrdersSum.get(Active)
        val deliveredOrders = state.ordersGroupedByStateWithOrdersSum.get(Delivered)

        activeOrders must not be None
        deliveredOrders must not be None

        activeOrders.get.sum must equalTo(650)
        logger.debug("" + activeOrders.get)
        deliveredOrders.get.sum must equalTo(4900)
        logger.debug("" + deliveredOrders.get)
        activeOrders.get.productsGroupedByproductId.apply(2) must not be empty

        deliveredOrders.get.productsGroupedByproductId.apply(1) must not be empty

      }
    }

    "current state of table should not return in case of bad input" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        BarTables.getTableStateFull(11111) must throwA[IllegalArgumentException]
      }
    }

    "table should have a name" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val table = BarTables.findById(1)

        table.get.name must equalTo("1-es")

      }
    }

    "table list should contain guests, order sum and order status" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        implicit val bar = Mocks.bars("mybar")
        val tableList = BarTables.listTablesWithState

        def hasUsersAndOrderSum(tableState: BarTableWithState) {
          tableState.guests.length must be > 1
          tableState.ordersGroupedByStateWithOrdersSum.apply(Delivered).sum must be > 1L
        }
        tableList.map(_ => hasUsersAndOrderSum(_))
        1 must equalTo(1)
      }
    }

    "user should be able to checkin into a table" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val user = Guests.findById(3).get
        val table = BarTables.findById(1).get

        BarTables.checkinUserToTable(user.id.get, table.id.get)

        val seat = Seats.findByGuest(user.id.get)

        seat must not be None
        seat.get.tableId must be equalTo table.id.get
      }
    }

    "not existent user shouldn't be able to checkin into a table" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        BarTables.checkinUserToTable(1, 77777) must throwA[IllegalArgumentException]
        BarTables.checkinUserToTable(898989, 1) must throwA[IllegalArgumentException]
      }
    }

    "user should be able to checkout from a table" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val user = Guests.findById(3).get
        val table = Guests.findById(1).get

        BarTables.checkinUserToTable(user.id.get, table.id.get)

        BarTables.checkoutUserFromTable(user.id.get)

        val seat = Seats.findByGuest(user.id.get)

        seat must equalTo(None)
      }
    }

    "a bar host should be set orders to paid on a table" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        BarTables.payOrdersOnTable(1)

        database.withSession {
          val unpaidOrderCountQ =
            (for {
              (guests, orders) <- BarTables.guests(1) join Orders on (_.id === _.guestId)
              if (!orders.delivered.isNull && orders.paid.isNull)
            } yield (orders))
          val unpaidOrderCount = Query(unpaidOrderCountQ.length).first
          unpaidOrderCount must equalTo(0)
        }
      }
    }

    "a bar host should be set orders to delivered on a table" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        BarTables.deliverOrdersOnTable(1)

        val undeliverOrderCount = database.withSession {
          (for {
            (guests, orders) <- BarTables.guests(1) join Orders on (_.id === _.guestId)
            if (orders.delivered.isNull)
          } yield (orders.length)).firstOption
        }

        undeliverOrderCount must equalTo(None)
      }
    }

    "a guest should be checked if he/she has taken a seat" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val hasSeat = Seats.hasGuestSeat(1)
        hasSeat must be equalTo (true)
      }
    }

    "a guest should be checked if he/she has taken a seat and should return false if there's no seat associated to him/her " in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val hasSeat = Seats.hasGuestSeat(11111)
        hasSeat must be equalTo (false)
      }
    }

    "a host should be able to list all the seats of the table " in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val seats = Seats.findByTable(1)
        seats.size must be equalTo (2)
      }
    }

    "a host should be able to checkout all guests from a table " in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        BarTables.checkoutTable(1)

        val table = BarTables.findByIdOrThrowIfNot(1)

        val seats = Seats.findByTable(1)
        seats.length must be equalTo (0)

        val tableState = BarTables.getTableStateFull(1)
        tableState.guests.length must be equalTo (0)
        tableState.ordersGroupedByStateWithOrdersSum.values.isEmpty must be equalTo (true)
      }
    }

    "a seated guest doesn't want to pay by default " in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        
        Seats.findByGuest(1).get.wantsToPay must beFalse
      }
    }
    
    "a guest must be able to indicate to the bar that he/she wants to pay " in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        
        BarTables.guestWantsToPay(1)
        Seats.findByGuest(1).get.wantsToPay must beTrue
      }
    }
  }

}