package test
import play.api.db._
import play.api.Play.current
import play.api.Play
import play.api.db.slick._
import play.api.db.slick.Config.driver.simple._
import org.specs2.mutable._
import play.api.test._
import play.api.test.Helpers._
import models._
import scala.slick.session.Database.threadLocalSession
import utils.Logger
import mocks.Mocks
import scala.util.Failure
import scala.util.Success
import scala.util.Failure
import scala.util.Failure

class BarHostSpec extends Specification with Logger with hasDB {

  import models._

  val testBar = Bar(name = "uj kocsma", address = "1119 Budafoki ut 100", verified = false)
  val testHostUser = User(name = "new host", email = "testhost@gmail.com", googleId = "843360392", facebookId = "facebook")

  "Host model" should {

    "be able to list the hosts of a bar" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val bid = Bars.insert(testBar)
        val uid = Guests.insert(testHostUser)
        val hid = Hosts.insert(Host(userId = uid, barId = bid))
        val bar = Bars.findByIdOrThrowIfNot(bid)
        Bars.listHosts(bar) must not be equalTo(Nil)
      }
    }

    "be able to list the admins of a bar" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val bid = Bars.insert(testBar)
        val admin = testHostUser.copy(true)
        val uid = Guests.insert(testHostUser)
        val hid = Hosts.insert(Host(userId = uid, barId = bid))
        val bar = Bars.findByIdOrThrowIfNot(bid)
        Bars.listHosts(bar) must not be equalTo(Nil)
      }
    }

    "be able to add a new host to a bar" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val bid = Bars.insert(testBar)
        val admin = testHostUser.copy(true)
        val uid = Guests.insert(testHostUser)
        val hid = Hosts.insert(Host(userId = uid, barId = bid))
        val bar = Bars.findByIdOrThrowIfNot(bid)

        val newHost = User(name = "new host 2", email = "testhost2@gmail.com", googleId = "1843360392", facebookId = "facebook")
        val newHostId = Guests.insert(newHost)
        Bars.addHost(newHostId)(bar)

        Hosts.listAll.filter(h => (h.userId == newHostId) && (h.barId == bid) && (!h.barAdmin)) must not be equalTo(Nil)
      }
    }

    "be able to add a new admin to a bar" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val bid = Bars.insert(testBar)
        val admin = testHostUser.copy(true)
        val uid = Guests.insert(testHostUser)
        val hid = Hosts.insert(Host(userId = uid, barId = bid))
        val bar = Bars.findByIdOrThrowIfNot(bid)

        val newHost = User(name = "new host 2", email = "testhost2@gmail.com", googleId = "1843360392", facebookId = "facebook")
        val newHostId = Guests.insert(newHost)
        Bars.addAdmin(newHostId)(bar)

        Hosts.listAll.filter(h => (h.userId == newHostId) && (h.barId == bid) && (h.barAdmin)) must not be equalTo(Nil)
      }
    }

    "be able to remove a host from a bar" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        
        val testBarId = Bars.insert(testBar)
        val adminUser = testHostUser.copy(true)
        val guestUserId = Guests.insert(testHostUser)
        val guestAsAdminId = Hosts.insert(Host(userId = guestUserId, barId = testBarId, barAdmin = true))
        val bar = Bars.findByIdOrThrowIfNot(testBarId)

        val newHostUser = User(name = "new host 2", email = "testhost2@gmail.com", googleId = "1843360392", facebookId = "facebook")
        val newHostUserId = Guests.insert(newHostUser)
        
        val newHostId = Hosts.insert(Host(userId = newHostUserId, barId = testBarId))

        val result = Bars.removeHost(newHostUserId)(bar)
        result match {
          case Success(deletedRows) => deletedRows must be equalTo (1)
          case Failure(t) => logger.error(t.getMessage()); failure
        }
        Hosts.listAll.filter(h => (h.userId == newHostUserId) && (h.barId == testBarId)) must be equalTo (Nil)
      }
    }

    "be able to remove an admin from a bar" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val barId = Bars.insert(testBar)
        val admin = testHostUser.copy(true)
        val testHostUserId = Guests.insert(testHostUser)
        val testHostId = Hosts.insert(Host(userId = testHostUserId, barId = barId, barAdmin = true))
        val bar = Bars.findByIdOrThrowIfNot(barId)

        val newHostUser = User(name = "new host 2", email = "testhost2@gmail.com", googleId = "1843360392", facebookId = "facebook")
        val newHostUserId = Guests.insert(newHostUser)
        val newHostId = Hosts.insert(Host(userId = newHostUserId, barId = barId, barAdmin = true))

        Bars.removeAdmin(newHostUserId)(bar)

        Hosts.listAll.filter(h => (h.userId == newHostUserId) && (h.barId == barId) && (h.barAdmin)) must be equalTo (Nil)
        Hosts.listAll.filter(h => ((h.barId == barId) && (h.barAdmin))) must not be equalTo(Nil)
        Hosts.listAll.filter(h => h.userId == newHostUserId) must not be equalTo(Nil)
      }
    }

    "not be able to remove an admin from a bar if he is the last host" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val barId = Bars.insert(testBar)
        val admin = testHostUser.copy(true)
        val testHostUserId = Guests.insert(testHostUser)
        val testHostId = Hosts.insert(Host(userId = testHostUserId, barId = barId, barAdmin = true))
        val bar = Bars.findByIdOrThrowIfNot(barId)

        Bars.removeAdmin(testHostUserId)(bar) must beFailedTry.withThrowable[IllegalArgumentException]
      }
    }

    "not be able to remove a host from a bar if he is the last host" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val bid = Bars.insert(testBar)
        val admin = testHostUser.copy(true)
        val uid = Guests.insert(testHostUser)
        val hid = Hosts.insert(Host(userId = uid, barId = bid, barAdmin = true))
        val bar = Bars.findByIdOrThrowIfNot(bid)

        Bars.removeHost(uid)(bar) must beFailedTry.withThrowable[IllegalArgumentException]
      }
    }

    "not be able to add a host twice" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val testbarId = Bars.insert(testBar)
        val admin = testHostUser.copy(true)
        val testHostUserId = Guests.insert(testHostUser)
        val testHostHostId = Hosts.insert(Host(userId = testHostUserId, barId = testbarId, barAdmin = true))
        val bar = Bars.findByIdOrThrowIfNot(testbarId)

        Bars.addHost(testHostUserId)(bar) must beEqualTo(testHostHostId)
      }
    }

    "not be able to add an admin twice" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val testbarId = Bars.insert(testBar)
        val admin = testHostUser.copy(true)
        val testHostUserId = Guests.insert(testHostUser)
        val testHostAsAdminId = Hosts.insert(Host(userId = testHostUserId, barId = testbarId, barAdmin = true))
        val bar = Bars.findByIdOrThrowIfNot(testbarId)

        Bars.addAdmin(testHostUserId)(bar) must beEqualTo(testHostAsAdminId)
      }
    }

  }
}