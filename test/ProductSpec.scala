package test

import scala.slick.driver.H2Driver.simple._
import org.specs2.mutable._
import play.api.test._
import play.api.test.Helpers._
import models._
import play.api.db._
import play.api.Play.current
import Database.threadLocalSession
import mocks.Mocks._
import org.h2.jdbc.JdbcSQLException
import org.specs2.matcher.BeEqualTo
import utils.Logger

class ProductSpec extends Specification with Logger {

  import models._

  // -- Date helpers

  def dateIs(date: java.util.Date, str: String) = new java.text.SimpleDateFormat("yyyy-MM-dd").format(date) == str
  val hamburgerName = "hamburger"
  val hamburgerPrice = 1200

  // --

  "Product model" should {

    "be retrieved by id" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val Some(hamburger) = Products.findById(1)

        hamburger.name must equalTo(hamburgerName)
        hamburger.price must equalTo(hamburgerPrice)

      }
    }

    "be listed " in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val products = Products.listAll

        products.size must equalTo(MProducts.values.toList.length)
      }
    }

    "be created " in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val products = Products.listAll
        val size = products.size
        Products.insert(Product(name = "hambi", description = "desc", price = 1000, availability = false))
        val productsWithNew = Products.listAll
        productsWithNew.size must equalTo(size + 1)
      }
    }

    "be updated if needed" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        Products.update(1, Product(name = "hambi", description = "desc", price = 1000, availability = false))

        val Some(hamburger) = Products.findById(1)

        hamburger.name must equalTo("hambi")
        hamburger.price must equalTo(1000)

      }
    }

    "be deleted if needed" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        Products.delete(10)
        1 must be equalTo 1
      }
    }

    "not be deleted if it is connected to other entities " in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        Products.isDeletable(1) must beFalse
        Products.delete(1) must throwA[Exception]
      }
    }

    "be able to filter product by a single tag " in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val products = Products.listByTag(Tag(None, "étel"))
        products.length must equalTo(4)
      }
    }

    "be able to filter product by a single tag and return empty if there are no tagged products " in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val products = Products.listByTag(Tag(None, "bor"))
        products.length must equalTo(0)
      }
    }

    "be able to get a product with tags " in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        
        val pho = Products.productWithTags(MProducts("pho").id.get).get
        
        val hamburger = Products.productWithTags(MProducts("hamburger").id.get).get
        hamburger.product.name must equalTo(MProducts("hamburger").name)
        hamburger.tags must contain(Tag(Some(1), "étel"))
        hamburger.tags must contain(Tag(Some(4), "hamburger"))
      }
    }

    "not be able to get a not existing product with tags " in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val fake = Products.productWithTags(1111)
        fake must beEqualTo(None)
      }
    }

    "not be able to insert duplicated tags " in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        Tags.insert(Tag(None, "duplicate"))
        Tags.insert(Tag(None, "duplicate")) must throwA[JdbcSQLException]
      }
    }

    "be able to insert new tags " in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val tagsLengthBefore = Tags.countRows
        val tag = Tags.findOrInsertNew(Tag(None, "mynewtag"))
        val tagsLengthAfter = Tags.countRows
        tagsLengthAfter must beEqualTo(tagsLengthBefore + 1)
      }
    }

    "be able to return existing tag when trying to insert a duplicate " in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val newId = Tags.insert(Tag(None, "mynewtag"))
        val tagsLengthBefore = Tags.countRows
        val tag = Tags.findOrInsertNew(Tag(None, "mynewtag"))
        val tagsLengthAfter = Tags.countRows
        tagsLengthAfter must beEqualTo(tagsLengthBefore)
        newId must beEqualTo(tag.id.get)
      }
    }

    "be able to tag a product" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val productBefore = Products.productWithTags(1)
        logger.debug("Tags before {}", productBefore.get.tags)

        val newTagList = List(
          Tag(None, "étel"), Tag(None, "meleg"), Tag(None, "finom"), Tag(None, "dijnyertes"), Tag(None, "marhahús"))

        val productWithTags = ProductWithTags(MProducts("hamburger"), newTagList)
        Products.updateWithTags(1, productWithTags)

        val productAfter = Products.productWithTags(1)
        logger.debug("Tags after {}", productAfter.get.tags)

        productAfter.get.tags.length must beEqualTo(newTagList.length)

        productAfter.get.tags must not contain (Tag(None, "hamburger"))
        productAfter.get.tags must contain(Tag(None, "étel"))
      }
    }

    "be able to tag a new product" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        val productToInsert = Product(Some(222), "fagyi", "leírás 222", 20, true)
        val newTagList = List(
          Tag(None, "étel"), Tag(None, "meleg"), Tag(None, "finom"), Tag(None, "dijnyertes"), Tag(None, "marhahús"))

        val after = Products.insertWithTags(ProductWithTags(productToInsert, newTagList))

        val productAfter = Products.productWithTags(after.product.id.get)
        logger.debug("Tags after {}", productAfter.get.tags)

        productAfter.get.tags.length must beEqualTo(newTagList.length)

        productAfter.get.tags must contain(Tag(None, "étel"))
      }
    }
  }
}