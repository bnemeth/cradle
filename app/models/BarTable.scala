package models

import play.api.Play.current
import play.api.db.slick._
import play.api.db.slick.Config.driver.simple._

import models._
import utils.Logger
import org.joda.time.DateTime
import models.mapper.JodaTimeConverter._
import java.sql.Date
import scala.slick.session.Database.threadLocalSession

case class BarTable(id: Option[Long] = None, name: String, barId: Long)

/**
 * A DTO with:
 * 1 the table
 * 2 the guests and their sum of delivered orders (what they pay in the end)
 * 3 orders grouped by state (active, delivered, paid)
 */
case class BarTableWithState(barTable: BarTable, guests: Seq[GuestWithDeliveredOrderSum], ordersGroupedByStateWithOrdersSum: Map[OrderState, OrdersSumWithProductList])

/**
 * A DTO with:
 * 1 a product map with the pr
 */
case class OrdersSumWithProductList(productsGroupedByproductId: Map[Long, List[Product]], latestOrderAdded: DateTime) {

  val sum: Long = productsGroupedByproductId.values.toList.flatten.foldLeft(0L)(_ + _.price)

  val productsWithNameCountAndSum = productsGroupedByproductId.map { productMapEntry =>
    (productMapEntry._2.head.name, productMapEntry._2.length, productMapEntry._2.length * productMapEntry._2.head.price)
  }.toList
}

object BarTables extends CRUDModel[BarTable]("BAR_TABLES") with Logger {

  def id = column[Long]("ID", O.PrimaryKey, O AutoInc) // This is the primary key column
  def name = column[String]("NAME")
  def barId = column[Long]("BAR_ID")

  def * = id.? ~ name ~ barId <> (BarTable.apply _, BarTable.unapply _)

  def bar = foreignKey("BAR_FK", barId, Bars)(_.id)

  def seatsOfTable(tableId: Long) = {
    val seat = (for {
      seat <- Seats
      if (seat.tableId === tableId)
    } yield (seat))
    seat
  }

  def guests(tableId: Long) = {
    val guest = (for {
      (seat, guest) <- seatsOfTable(tableId) join Guests on (_.guestId === _.id)
    } yield (guest))
    logger.trace("Guests on table: " + guest.list.toString)
    guest
  }

  /**
   * Check in user to a table
   * Prerequisite: he's not sitting anywhere else
   */
  def checkinUserToTable(guestId: Long, tableId: Long) {
    logger.info("checkinUserToTable, guest: {}, table: {}", guestId, tableId)
    database.withTransaction {
      val guest = Guests.findByIdOrThrowIfNot(guestId)
      val table = BarTables.findByIdOrThrowIfNot(tableId)
      checkoutUserFromTable(guestId)
      val seat = Seat(None, tableId, guestId, new DateTime)
      Seats.autoInc.insert(seat)
      BarFeed.default ! Checkin(guest, table)
    }
  }

  /**
   * Checkout a guest from its table
   * Prerequisites: user has paid all its orders (method might send an alert to bar)
   */
  def checkoutUserFromTable(guestId: Long) {

    val guestFound = Guests.findByIdOrThrowIfNot(guestId)

    database.withTransaction {
      if (Seats.hasGuestSeat(guestId)) {
        val seat = Seats.findByGuest(guestFound.id.get).get
        Seats.delete(seat.id.get)
        logger.info("User {} checkout from table {}", guestFound.id.get, seat.tableId)
      } else {
        logger.warn("User {} tries to checkout from a seat he was not seated on", guestId)
      }
    }
  }

  /**
   * Checkout the whole table
   * Prerequisites: no.
   * A host is always able to clear a table
   * Meaning:
   * 1 all delivered orders are set to paid
   * 2 all active orders are deleted
   * 3 all guest seats are deleted
   */
  def checkoutTable(tableId: Long) = {
    database.withTransaction {

      val table = BarTables.findByIdOrThrowIfNot(tableId)
      val seats = Seats.findByTable(tableId)

      payOrdersOnTable(tableId)
      revokeActiveOrdersOnTable(tableId)
      Query(Seats).filter(seat => seat.tableId === tableId).delete
    }
    BarFeed.default ! OrderEvent
  }

  /**
   * A guest indicates to the bar that he/she wants to pay
   */
  def guestWantsToPay(guestId: Long) = database.withSession {
    logger.debug("guest {} wants to pay", guestId)
    if (Orders.getDeliveredOrdersOfGuest(guestId).length > 0) {
      logger.debug("delivered orders: {}", Orders.getDeliveredOrdersOfGuest(guestId))
      Query(Seats).filter(_.guestId === guestId).map(_.wantsToPay).update(true)
      BarFeed.default ! WantsToPay(Guests.findByIdOrThrowIfNot(guestId))
    } else {
      logger.warn("Guest {} wants to pay, although he doesn't have any orders", guestId)
    }
  }

  /**
   * Returns the current orders by state and their sum and the sitting guest list on the table
   */
  def getTableStateFull(tableId: Long) = database.withTransaction {

    val table = findByIdOrThrowIfNot(tableId)

    val tableState =
      for {
        (guests, orders) <- guests(tableId) leftJoin Orders on (_.id === _.guestId)
      } yield (guests, orders.maybe)

    logger.trace("tableState {}", tableState.selectStatement)

    val tupleOfGuestsAndOrders = tableState.list.unzip
    val ordersList = tupleOfGuestsAndOrders._2.filter(isNotEmptyAndNotPaidOrder).map(_.get)
    val ordersWithProducts = ordersList.map(order => OrderWithProducts(order, order.products, order.sum))

    logger.trace("ordersWithProducts: {}", ordersWithProducts)

    val ordersGroupedByState = ordersWithProducts.groupBy(_.order.state).map(extractListOfProductsAndLatestOrderTimeFromOrders)
    val deliveredOrdersGroupedByGuestId = ordersWithProducts.groupBy(_.order.guestId).map(extractDeliveredOrderSum)
    val deliveredOrdersGroupedByGuest = tupleOfGuestsAndOrders._1.distinct.map(user =>
      GuestWithDeliveredOrderSum(user, deliveredOrdersGroupedByGuestId.getOrElse(user.id.get, 0L), Seats.findByGuest(user.id.get).get.wantsToPay))

    val ordersGroupedByStateWithProductListAndSumOfAll = ordersGroupedByState.map(convertToOrdersSumWithProductList)
    BarTableWithState(table, deliveredOrdersGroupedByGuest, ordersGroupedByStateWithProductListAndSumOfAll)
  }

  private def extractListOfProductsAndLatestOrderTimeFromOrders(ordersGroupedByState: (OrderState, List[models.OrderWithProducts])): (OrderState, (Map[Long, List[Product]], DateTime)) = {
    ordersGroupedByState._1 -> (ordersGroupedByState._2.foldLeft(List[Product]())(_ ++ _.products).groupBy(_.id.get), latestOrderTime(ordersGroupedByState._2))
  }

  private def convertToOrdersSumWithProductList(ordersGroupedByState: (OrderState, (Map[Long, List[Product]], DateTime))): (OrderState, OrdersSumWithProductList) = {
    ordersGroupedByState._1 -> OrdersSumWithProductList(ordersGroupedByState._2._1, ordersGroupedByState._2._2)
  }

  private def isNotEmptyAndNotPaidOrder(order: Option[Order]): Boolean = {
    logger.trace("Order to filter: {}", order)
    order match {
      case Some(order) => order.products.length > 0 && order.state != Paid
      case _ => false
    }
  }

  private def extractDeliveredOrderSum(ordersGroupedByGuestId: (Long, List[models.OrderWithProducts])): (Long, Long) = {
    ordersGroupedByGuestId._1 -> ordersGroupedByGuestId._2.filter(_.order.state == Delivered).map(_.sum).sum
  }

  /**
   * Get the time of the latest order
   */
  def latestOrderTime(orders: List[OrderWithProducts]) = {
    orders.sorted(Ordering.by((_: OrderWithProducts).order.created).reverse).head.order.created
  }

  /**
   * Returns the active orders sum and the sitting guest list on all the tables
   */
  def listTablesWithState(implicit bar: Bar): List[BarTableWithState] = database.withSession {
    logger.debug("listTablesWithState entry...")
    val tables = Query(BarTables).filter(_.barId === bar.id).list.map(table => (BarTables.getTableStateFull(table.id.get)))
    logger.trace(tables.toString)
    tables
  }

  /**
   * Returns the tables in a bar to display for the guest
   */
  def listTablesOfBar(barId: Long): List[BarTableWithState] = database.withSession {
    val bar = Bars.findByIdOrThrowIfNot(barId)
    listTablesWithState(bar)
  }

  /**
   * Set all orders to delivered on the table by id
   */
  def deliverOrdersOnTable(tableId: Long) = database.withTransaction {

    logger.debug("deliverOrdersOnTable {} entry...", tableId);

    val table = findByIdOrThrowIfNot(tableId)

    val undeliveredOrderIdList =
      (for {
        (guests, orders) <- guests(tableId) join Orders on (_.id === _.guestId)
        if (orders.delivered.isNull)
      } yield (orders.id)).list

    logger.debug("undeliveredOrderIdList: {}", undeliveredOrderIdList)

    undeliveredOrderIdList.foreach { recordToUpdateId =>
      val recordsToUpdate = for {
        orders <- Orders
        if (orders.id === recordToUpdateId)
      } yield orders.delivered
      recordsToUpdate.update(Some(new DateTime()))
    }
    BarFeed.sendOrderEvent
  }

  /**
   * Delete all active orders on the table by id
   */
  def revokeActiveOrdersOnTable(tableId: Long) = database.withTransaction {

    logger.debug("revokeActiveOrdersOnTable {} entry...", tableId);

    val table = findByIdOrThrowIfNot(tableId)

    val undeliveredOrderIdList =
      (for {
        (guests, orders) <- guests(tableId) join Orders on (_.id === _.guestId)
        if (orders.delivered.isNull)
      } yield (orders.id)).list

    logger.debug("undeliveredOrderIdList: {}", undeliveredOrderIdList)

    undeliveredOrderIdList.foreach { recordToDeleteId =>
      val recordToDelete = for {
        orders <- Orders
        if (orders.id === recordToDeleteId)
      } yield orders
      Query(LineItems).filter(lineItem => lineItem.orderId === recordToDeleteId).delete
      recordToDelete.delete
    }
  }

  /**
   * Set all orders to paid on the table by id
   */
  def payOrdersOnTable(tableId: Long) = database.withTransaction {

    logger.debug("payOrdersOnTable {} entry...", tableId);

    val table = findByIdOrThrowIfNot(tableId)

    val unpaidOrderIdList =
      (for {
        (guests, orders) <- guests(tableId) join Orders on (_.id === _.guestId)
        if (!orders.delivered.isNull && orders.paid.isNull)
      } yield (orders.id)).list

    logger.debug("unpaidOrderIdList: {}", unpaidOrderIdList)

    unpaidOrderIdList.foreach { recordToUpdateId =>
      val recordsToUpdate = for {
        orders <- Orders
        if (orders.id === recordToUpdateId)
      } yield orders.paid
      recordsToUpdate.update(Some(new DateTime()))
    }

    resetWantsToPayRequests(tableId)

    BarFeed.default ! OrderEvent
  }

  private def resetWantsToPayRequests(tableId: Long) {
    Query(Seats).filter(_.tableId === tableId).map(_.wantsToPay).update(false)
  }

  def getCurrentTableState(guestId: Long) = {
    val table = Guests.getCurrentTable(guestId)
    getTableStateFull(table.id.get)
  }

}
