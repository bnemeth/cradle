package models

import scala.concurrent.duration._
import Formats._
import akka.actor._
import akka.pattern.ask
import akka.util.Timeout
import play.api._
import play.api.Play.current
import play.api.libs.concurrent._
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.iteratee._
import play.api.libs.json._
import play.api.libs.iteratee.Concurrent.Channel
import scala.concurrent.Future

object BarFeed {

  implicit val timeout = Timeout(1 second)

  lazy val default = {
    val feedActor = Akka.system.actorOf(Props[BarFeed])
    feedActor
  }

  def init: scala.concurrent.Future[(Iteratee[JsValue, _], Enumerator[JsValue])] = {
    Future {
      val (out, channel) = Concurrent.broadcast[JsValue]
      val in = Iteratee.foreach[JsValue] {
        msg =>
          channel.push(JsString(""))
      }
      BarFeed.default ! Connected(channel)
      (in, out)
    }
  }
  
  def sendOrderEvent() = {
    default ! OrderEvent
  }
}

class BarFeed extends Actor with utils.Logger {

  var out: Option[Channel[JsValue]] = None

  def receive = {

    case Connected(enumerator: Channel[JsValue]) => {
      out = Some(enumerator)
    }

    case Checkin(guest, barTable) => {
      val msg = JsObject(
        Seq(
          "kind" -> JsString("checkin"),
          "guest" -> Json.toJson(guest),
          "barTable" -> Json.toJson(barTable)))

      notifyBar(msg)
      logger.debug("User checked in {}", guest.name)
    }

    case OrderEvent => {
      val msg = JsObject(
        Seq(
          "kind" -> JsString("order")))

      notifyBar(msg)
    }
    
    case WantsToPay(guest) => {
      val msg = JsObject(
        Seq(
          "kind" -> JsString("wantstopay"),
          "guest" -> Json.toJson(guest)))

      notifyBar(msg)
    }

    case Checkout(guest, barTable) => {
      val msg = JsObject(
        Seq(
          "kind" -> JsString("checkout"),
          "guest" -> Json.toJson(guest),
          "barTable" -> Json.toJson(barTable)))

      notifyBar(msg)
      logger.debug("User checked out {}", guest.name)
    }

  }

  def notifyBar(msg: JsObject) {

    out match {
      case Some(_) => out.get.push(msg)
      case None => logger.debug("message received but couldn't be delivered")
    }

  }

}

case class Checkin(guest: User, barTable: BarTable)
case class Checkout(guest: User, barTable: BarTable)
case class OrderEvent()
case class WantsToPay(guest: User)
case class Connected(enumerator: Channel[JsValue])
