package models

import play.api.db._
import play.api.db.slick._
import play.api.db.slick.Config.driver.simple._
import play.api.Play.current
import scala.slick.session.Database.threadLocalSession

import models._

case class Product(id: Option[Long] = None, name: String, description: String, price: Long, availability: Boolean) {
  override def toString = name + " " + price + " Ft"
}

case class ProductWithTags(product: Product, tags: List[Tag]) {
  override def toString = product.toString + " tags: " + tags.map(_.toString)
}

object ProductWithTags {
  def apply2(id: Option[Long], name: String, description: String, price: Long, availability: Boolean, tags: String): ProductWithTags = {
    val tagList = tags.split(",").toList.map(Tag(None, _))
    ProductWithTags(Product(id, name, description, price, availability), tagList)
  }

  def unapply2(productWithTags: ProductWithTags): Option[(Option[Long], String, String, Long, Boolean, String)] = {
    Product.unapply(productWithTags.product).get match {
      case (a, b, c, d, e) => Some(a, b, c, d, e, productWithTags.tags.map(_.name).mkString(","))
    }
  }
}

object Products extends CRUDModel[Product]("PRODUCTS") {

  def id = column[Long]("ID", O.PrimaryKey, O.AutoInc) // This is the primary key column
  def name = column[String]("NAME")
  def description = column[String]("DESC")
  def price = column[Long]("PRICE") //should be BigDecimal if other currency than HUF is supported
  def availability = column[Boolean]("AVAILABILITY")

  def * = id.? ~ name ~ description ~ price ~ availability <> (Product.apply _, Product.unapply _)

  /**
   * Update a product
   * @param id
   * @param product
   */
  def update(id: Long, product: Product) {
    database.withTransaction {
      val productToUpdate: Product = product.copy(Some(id))
      Products.where(_.id === id).update(productToUpdate)
    }
  }

  /**
   * Update a product and save the tags along
   * @param id
   * @param productWithTags
   */
  def updateWithTags(id: Long, productWithTags: ProductWithTags) {
    database.withSession {
      val productToUpdate: Product = productWithTags.product.copy(Some(id))
      Products.where(_.id === id).update(productToUpdate)
      addRemoveTags(id, productWithTags.tags)
    }
  }

  /**
   * Insert a product and save the tags along
   * @param productWithTags
   */
  def insertWithTags(productWithTags: ProductWithTags) = database.withTransaction {
    val productId = Products.insert(productWithTags.product)
    logger.debug("{}",productId)
    addRemoveTags(productId, productWithTags.tags)
    Products.productWithTags(productId).get
  }

  private def addRemoveTags(productId: Long, newTags: List[Tag]) = {

    val productWithTags = Products.productWithTags(productId).get

    val newTagNames = newTags.map(_.name)
    logger.debug("new names {}", newTagNames)
    val oldTagNames = productWithTags.tags.map(_.name)
    logger.debug("oldTagNames {}", oldTagNames)
    val union = newTagNames union oldTagNames
    logger.debug("union {}", union)
    val addNewNames = union.filterNot(oldTagNames.toSet)
    val removeOldNames = union.filterNot(newTagNames.toSet)

    logger.debug("addNewNames {}", addNewNames)
    logger.debug("removeOldNames {}", removeOldNames)

    addNewNames.map(name => addTag(productWithTags.product.id.get, Tag(None, name)))
    removeOldNames.map(name => removeTag(productWithTags.product.id.get, Tag(None, name)))
  }

  private def addTag(productId: Long, tag: Tag) {
    val foundTag = Tags.findOrInsertNew(tag)
    ProductTags.insert(ProductTag(None, productId, foundTag.id.get))
  }

  private def removeTag(productId: Long, tag: Tag) {
    val foundTag = Tags.findOrInsertNew(tag)
    ProductTags.where(productTag => (productTag.productId === productId) && (productTag.tagId === foundTag.id.get)).delete
  }

  /**
   * Checks if a product can be deleted -> has no connected entities
   * Later on there are 2 solutions instead of that
   * 1 introduce a deleted flag
   * 2 do not allow deletion at all but use the availability flag
   */
  def isDeletable(id: Long): Boolean = database.withSession {
    Query(
      (for {
        lineItem <- LineItems
        if (lineItem.productId === id)
      } yield lineItem).length).first == 0
  }

  /**
   * Retrieve a product with all its tags
   */
  def productWithTags(productId: Long): Option[ProductWithTags] = database.withSession {
    val query = for {
      ((product, productTag), tag) <- Products leftJoin ProductTags on (_.id === _.productId) leftJoin Tags on (_._2.tagId === _.id)
      if (product.id === productId)
    } yield (product, tag.maybe)
    
    val tuple = query.list
    if (tuple.isEmpty) {
      None
    } else {
      val product = tuple.head._1
      val tags = tuple.map(_._2).flatten
      Some(ProductWithTags(product, tags))
    }
  }

  /**
   * Filter products by a single tag
   */
  def listByTag(filterTag: Tag): List[Product] = database.withSession {
    val query = for {
      ((tag, productTag), product) <- Tags join ProductTags on (_.id === _.tagId) join Products on (_._2.productId === _.id)
      if (tag.name === filterTag.name)
    } yield product
    query.list
  }
}