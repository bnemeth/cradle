package models.mapper

import slick.lifted.MappedTypeMapper
import java.sql.Timestamp
import org.joda.time.DateTime
import slick.lifted.TypeMapper.DateTypeMapper
 
object JodaTimeConverter {
 
  implicit def date2dateTime = MappedTypeMapper.base[DateTime, Timestamp] (
    dateTime => new Timestamp(dateTime.getMillis),
    date => new DateTime(date)
  )

  implicit def dateTimeOrdering: Ordering[DateTime] = Ordering.fromLessThan(_ isBefore _)
}