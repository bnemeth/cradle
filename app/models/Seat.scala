package models


import play.api.db._
import play.api.Play.current
import play.api.db.slick._
import play.api.db.slick.Config.driver.simple._
import scala.slick.session.Database.threadLocalSession

import org.joda.time.DateTime
import mapper.JodaTimeConverter._
import utils.Logger

/**
 * Representing a checkin state of a user in a table 
 */
case class Seat(id: Option[Long], tableId: Long, guestId: Long, created: DateTime = new DateTime, wantsToPay: Boolean = false) {
  override def toString = "id: " + id + " table: " + tableId + " guest: " + guestId
}

object Seats extends CRUDModel[Seat]("SEATS") with Logger{
  def id = column[Long]("ID", O.PrimaryKey, O AutoInc) // This is the primary key colum
  def tableId = column[Long]("TABLE_ID")
  def guestId = column[Long]("GUEST_ID")
  def created = column[DateTime]("CREATED")
  def wantsToPay = column[Boolean]("WANTS_TO_PAY")

  def * = id.? ~ tableId ~ guestId ~ created ~ wantsToPay <> (Seat.apply _, Seat.unapply _)

  def guest = foreignKey("GUESTS_FK", guestId, Guests)(_.id)

  def table = foreignKey("TABLES_FK", tableId, BarTables)(_.id)

  def findByGuest(guestId: Long) = {
    database.withSession {
      Query(Seats).filter(seat => seat.guestId === guestId).firstOption
    }
  }

  def findByTable(tableId: Long) = {
    database.withSession {
      Query(Seats).filter(seat => seat.tableId === tableId).list
    }
  }
  
  /**
   * Checks if the guest has already a seat
   */
  def hasGuestSeat(guestId: Long) = {
    logger.trace("Checking if guest {} has taken a seat...", guestId)
    database.withSession {
      Query(Seats.filter(seat => seat.guestId === guestId).length > 0).first
    }
  }
}