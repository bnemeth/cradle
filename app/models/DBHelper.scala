package models

import play.api.db._
import play.api.Play.current
import play.api.db.slick._
import play.api.db.slick.Config.driver.simple._
import utils.Logger
import org.joda.time.DateTime
import java.util.Date
import scala.slick.session.Database.threadLocalSession
import global.MyResources._
import global._
import com.google.inject.Guice
import net.codingwell.scalaguice._
import com.google.inject._

trait hasDB {
  def database = Global.injector.instance[MyDB].database
}

/**
 * Helper for otherwise verbose Slick model definitions
 */
abstract class CRUDModel[T <: AnyRef { val id: Option[Long] }](schemaName: Option[String] = None, tableName: String)
  extends Table[T](schemaName, tableName) with Logger with hasDB {

  def this(tableName: String) = this(None, tableName)

  def id: Column[Long]

  def * : scala.slick.lifted.ColumnBase[T]

  def autoInc = * returning id

  def insert(entity: T) = {
    database.withTransaction {
      autoInc.insert(entity)
    }
  }

  def insertAll(entities: Seq[T]) {
    database.withTransaction {
      autoInc.insertAll(entities: _*)
    }
  }

  //  def update(id: Long, entity: T) {
  //    database.withSession {
  //      val q = tableQueryToUpdateInvoker(
  //        tableToQuery(this).where(_.id === id))
  //      logger.debug("e: {}", entity)
  //      logger.debug("q: {}", q.updateStatement)
  //      q.update(entity)
  //    }
  //  }

  def delete(id: Long) {
    database.withTransaction {
      queryToDeleteInvoker(
        tableToQuery(this).where(_.id === id)).delete
    }
  }

  def countRows = database.withSession {
    Query(tableToQuery(this).length).first
  }

  /**
   * List all entities
   */
  def listAll: List[T] = database.withSession {
    tableToQuery(this).list.map(t => t.asInstanceOf[T])
  }

  val byId = createFinderBy(_.id)

  /**
   * Retrieve an entity by id
   * @param id
   */
  def findById(id: Long): Option[T] =
    database.withSession {
      byId(id).firstOption
    }

  /**
   * Retrieve an entity by id or throw an IllegalArgumentException if it cannot be found
   * @param id
   */
  def findByIdOrThrowIfNot(id: Long): T = {
    findById(id).getOrElse(throw new IllegalArgumentException("Could not find " + this.getClass() + "with the given id " + id))
  }
}