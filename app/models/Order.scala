package models

import play.api.db._
import play.api.Play.current
import play.api.db.slick._
import play.api.db.slick.Config.driver.simple._
import scala.slick.session.Database.threadLocalSession

import org.joda.time.DateTime
import mapper.JodaTimeConverter._

import scala.slick.lifted.Projection5
import utils.Logger

case class Order(id: Option[Long], guestId: Long, created: DateTime = DateTime.now, delivered: Option[DateTime] = None, paid: Option[DateTime] = None) {
  
  def products: List[Product] = {
    (for {
      lineItem <- LineItems
      product <- lineItem.product
      if(lineItem.orderId === id)
    } yield (product)).list
  }

  def sum = {
    products.map(_.price).sum
  }
  
  def state: OrderState = {
    if(paid != None){Paid}
    else if(delivered != None){Delivered}
    else {Active}
  }
  
  override def toString = "Products: " + products.length + " Sum: " + sum
  def toLongString = "Guest: " + guestId + " Products: " + products + " Sum: " + sum
}

case class OrderWithProducts(order: Order, products: List[Product], sum: Long) {
  
  override def toString = "Products: " + products.length + " Sum: " + sum
  def toLongString = "Guest: " + order.guestId + " Products: " + products + " Sum: " + sum
}

sealed trait OrderState{def next: OrderState}
case object Active extends OrderState {val next = Delivered}
case object Delivered extends OrderState {val next = Paid}
case object Paid extends OrderState {def next = throw new IllegalStateException}

case class LineItem(id: Option[Long], productId: Long, orderId: Long)

//convenience dto for number of products ordered
case class Basket(productIdToOrderedCount: Map[Long,Long])

object Orders extends CRUDModel[Order]("ORDERS") with Logger{

  def id = column[Long]("ID", O.PrimaryKey, O AutoInc) // This is the primary key column
  def guestId = column[Long]("GUEST_ID")
  def created = column[DateTime]("CREATED")
  def delivered = column[Option[DateTime]]("DELIVERED")
  def paid = column[Option[DateTime]]("PAID")

  def * = id.? ~ guestId ~ created ~ delivered ~ paid <> (Order.apply _, Order.unapply _)
  
  /**
   * Used when Order is one the right side of a left join and the projected columns might return null
   */
  def maybe = id.? ~ guestId.?  ~ created.?  ~ delivered  ~ paid  <> (
      tupleToOrder _,
      (order: Option[Order]) => None
  )
  
  def tupleToOrder(orderTuple: (Option[Long],Option[Long],Option[DateTime],Option[DateTime],Option[DateTime])) : Option[Order]= orderTuple match {
  	case (Some(id),Some(guestId),Some(created),delivered,paid) => Some(Order(Some(id),guestId,created,delivered,paid))
  	case _ => None
  }
  
  def guest = foreignKey("GUEST_FK", guestId, Guests)(_.id)
  
  def placeOrder(guestId: Long, basket: Basket) = database.withTransaction{
	  val table = Guests.getCurrentTable(guestId);
	  
	  val newOrderId = Orders.insert(Order(None, guestId = guestId))
	  logger.debug("New order: {}", newOrderId)
	  basket.productIdToOrderedCount.map(productIdAndCount => 	    
	    LineItems.insertMany(LineItem(None,productIdAndCount._1,newOrderId),productIdAndCount._2)
	  )
	  BarFeed.default ! OrderEvent
  }
  
  private def getCurrentOrdersOfTableByGuest(guestId: Long) = {
    val tableWithState = BarTables.getCurrentTableState(guestId)
    tableWithState.ordersGroupedByStateWithOrdersSum
  }
  
  def getActiveOrdersOfTableByGuest(guestId: Long) = {
    val tableWithState = BarTables.getCurrentTableState(guestId)
    tableWithState.ordersGroupedByStateWithOrdersSum(Active)
  }
  
  def getDeliveredOrdersOfTableByGuest(guestId: Long) = {
    val tableWithState = BarTables.getCurrentTableState(guestId)
    tableWithState.ordersGroupedByStateWithOrdersSum(Delivered)
  }
  
  def getDeliveredOrdersOfGuest(guestId: Long) = database.withSession {
	(for{
		orders <- Orders
	  if(orders.guestId === guestId && !orders.delivered.isNull && orders.paid.isNull)
	} yield orders).list
  }
}

object LineItems extends CRUDModel[LineItem]("LINE_ITEM") with Logger{
  def id = column[Long]("ID", O.PrimaryKey, O AutoInc) // This is the primary key column
  def productId = column[Long]("PRODUCT_ID")
  def orderId = column[Long]("ORDER_ID")

  def * = id.? ~ productId ~ orderId <> (LineItem.apply _, LineItem.unapply _)
  
  def order = foreignKey("ORDER_FK", orderId, Orders)(_.id)
  def product = foreignKey("PRODUCT_FK", productId, Products)(_.id)
  
  /**
   * Insert the given lineItem count times
   */
  def insertMany(lineItem: LineItem, count: Long) = {
    logger.debug("LineItem and count: {} {}",lineItem, count)
  	1 to count.toInt foreach { _ => LineItems.insert(lineItem)}
  }
}