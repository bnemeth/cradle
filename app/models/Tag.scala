package models
import scala.slick.session.Database.threadLocalSession

case class Tag(id: Option[Long] = None, name: String) {
  override def toString = "#" + name
  override def equals(other: Any): Boolean = other match {
    case Tag(id, name) => this.name.equals(name)
    case _ => false
  }
  override def hashCode(): Int = {
    31 * this.name.hashCode()
  }
}

object Tags extends CRUDModel[Tag]("TAG") {

  def id = column[Long]("ID", O.PrimaryKey, O AutoInc) // This is the primary key column
  def name = column[String]("NAME")

  def nameIndex = index("name_idx", name, unique = true)

  def * = id.? ~ name <> (Tag.apply _, Tag.unapply _)
  
  def maybe = id.? ~ name.?  <> (
      tupleToTag _,
      (order: Option[Tag]) => None
  )
  
  def tupleToTag(tuple: (Option[Long], Option[String])): Option[Tag] = tuple match {
  	case (Some(id),Some(name)) => Some(Tag(Some(id),name))
  	case _ => None
  }

  /**
   * Inserts a new tag or returns an existing if there is a tag with the given name
   */
  def findOrInsertNew(tag: Tag): Tag = {
    val cleanTag = cleanName(tag)
    val found = findByName(cleanTag.name)
    found match {
      case Some(tag) => tag
      case None => Tag(Some(insert(cleanTag)), cleanTag.name)
    }
  }

  private def cleanName(tag: Tag) = {
    Tag(None, tag.name.trim)
  }

  val byName = createFinderBy(_.name)

  def findByName(name: String) = database.withSession {
    byName(name).firstOption
  }
}

case class ProductTag(id: Option[Long], productId: Long, tagId: Long)

object ProductTags extends CRUDModel[ProductTag]("PRODUCT_TAG") {

  def id = column[Long]("ID", O.PrimaryKey, O AutoInc) // This is the primary key column
  def productId = column[Long]("PRODUCT_ID")
  def tagId = column[Long]("TAG_ID")

  def product = foreignKey("PRODUCT_PT_FK", productId, Products)(_.id)
  def tag = foreignKey("TAG_PT_FK", tagId, Tags)(_.id)

  def * = id.? ~ productId ~ tagId <> (ProductTag.apply _, ProductTag.unapply _)
}

