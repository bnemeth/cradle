package models

import play.api.libs.json.Json

/**
 * Implicit formatters for JSON serializer
 */
object Formats {
  
  implicit val readsBarTable = Json.reads[BarTable]
  implicit val writesBarTable = Json.writes[BarTable]
  
  implicit val readsUser = Json.reads[User]
  implicit val writesUser = Json.writes[User]
    
  implicit val readsBar = Json.reads[Bar]
  implicit val writesBar = Json.writes[Bar]
  
}