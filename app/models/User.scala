package models

import play.api.db._
import play.api.Play.current
import play.api.db.slick._
import play.api.db.slick.Config.driver.simple._
import scala.slick.session.Database.threadLocalSession

import securesocial.core._
import securesocial.core.IdentityId
import utils.Logger
import play.api.libs.json._
import play.api.libs.functional.syntax._
import scala.reflect.runtime.{ universe => ru }

case class SocialWrappedUser(appUser: User) extends Identity {
  def identityId = appUser.identityId
  def firstName = appUser.name
  def lastName = appUser.name
  def fullName = appUser.name
  def email = Some(appUser.email)
  def avatarUrl = appUser.avatarUrl
  def authMethod = AuthenticationMethod.OAuth2
  def oAuth1Info = None
  def oAuth2Info = None
  def passwordInfo = None
}

case class User(id: Option[Long] = None, name: String, email: String, facebookId: String, googleId: String, avatarUrl: Option[String] = None) {

  override def toString = "Name: " + name + " (ID: " + id + ")"
  def toLongString = "Name: " + name + " (ID: " + id.get + ")"

  lazy val facebookIdentity = IdentityId(facebookId, "facebook")
  lazy val googleIdentity = IdentityId(googleId, "google")

  val profileUrl = facebookId match {
    case "" => "https://plus.google.com/" + googleId
    case _ => "http://facebook.com/" + facebookId
  }

  val identityId = facebookId match {
    case "" => googleIdentity
    case _ => facebookIdentity
  }
}

case class GuestWithDeliveredOrderSum(guest: User, sum: Long, wantsToPay: Boolean)

object Guests extends CRUDModel[User]("GUESTS") {

  def id = column[Long]("ID", O.PrimaryKey, O AutoInc) // This is the primary key column
  def name = column[String]("NAME")
  def email = column[String]("EMAIL")
  def facebookId = column[String]("FACEBOOK_ID")
  def googleId = column[String]("GOOGLE_ID")
  def avatarUrl = column[Option[String]]("AVATAR_URL")

  def * = id.? ~ name ~ email ~ facebookId ~ googleId ~ avatarUrl <> (User.apply _, User.unapply _)

  /**
   * Get the current table of guest
   */
  def getCurrentTable(guestId: Long) = {
    val guestFound = Guests.findByIdOrThrowIfNot(guestId);
    BarTables.findByIdOrThrowIfNot(Seats.findByGuest(guestId).get.tableId)
  }

  /**
   * Retrieve a guest by their socialId
   * @param socialId
   */
  def findBySocialId(identityId: IdentityId): Option[User] = database.withSession {
    identityId.providerId match {
      case "facebook" => Query(Guests).filter(_.facebookId === identityId.userId).firstOption
      case "google" => Query(Guests).filter(_.googleId === identityId.userId).firstOption
      case _ => None
    }
  }

  /**
   * Retrieve a user by their email
   */
  def findByEmail(email: String): Option[User] = database.withSession {
    Query(Guests).filter(_.email === email).firstOption
  }

  /**
   * Updates the user's record with one of its social ID
   */
  def updateSocial(guestId: Long, identityId: IdentityId) = database.withSession {

    logger.debug("Updating user {} with a new social ID {}", guestId, identityId)

    identityId.providerId match {
      case "facebook" => Query(Guests).filter(_.id === guestId).map(_.facebookId).update(identityId.userId)
      case "google" => Query(Guests).filter(_.id === guestId).map(_.googleId).update(identityId.userId)
      case _ => -11111
    }
    Guests.findByIdOrThrowIfNot(guestId)
  }

  implicit val reads = Json.reads[User]
  implicit val writes = Json.writes[User]
  
  /**
   * Filter users by a filter string on their name or email (match everywhere)
   */
  def listByFilter(filter: String): List[User] = database.withSession {
    logger.debug("Filtering users by {}", filter)
    val query = for {
      users <- Guests if ((users.name like "%"+filter+"%") || (users.email like "%"+filter+"%"))
    } yield users
    query.list
  }
}
/**
 * Very simple authorization system: if a userId is in the table, it's a host
 */
case class Host(id: Option[Long] = None, userId: Long, barId: Long, barAdmin: Boolean = false)
case class HostUser(host: Host, user: User) 
object Hosts extends CRUDModel[Host]("HOSTS") with Logger {

  def id = column[Long]("ID", O.PrimaryKey, O AutoInc) // This is the primary key column
  def userId = column[Long]("USER_ID")
  def barId = column[Long]("BAR_ID")
  def barAdmin = column[Boolean]("BAR_ADMIN")
  
  def * = id.? ~ userId ~ barId ~ barAdmin <> (Host.apply _, Host.unapply _)

  /**
   * Tells if a user is a host
   */
  def isHost(userId: Long) = database.withSession {
    val query = for {
      host <- Hosts
      if (host.userId === userId)
    } yield host
    Query(query.length).first > 0
  }
  
  /**
   * Tells if a user is an admin in a bar
   */
  def isBarAdmin(userId: Long) = database.withSession {
    val query = for {
      host <- Hosts
      if (host.userId === userId) && host.barAdmin
    } yield host
    Query(query.length).first > 0
  }
  
  /**
   * Returns the bar where the host is an employee
   */
  def getBarOptionOfHost(userId: Long) = database.withSession {
    logger.debug("lofasz {}", Bars.listAll)
    Hosts.listAll.map{ h =>
      logger.debug("hosztok {}", h)
    }
      val query = for {
        (host, bar) <- Hosts innerJoin Bars on (_.barId === _.id)
        if(host.userId === userId)
      } yield (bar)
      query.firstOption
  }
  
    /**
   * Returns the bar where the host is an employee
   */
  def getBarOfHost(userId: Long) = database.withSession {
      getBarOptionOfHost(userId).get
  }
}
