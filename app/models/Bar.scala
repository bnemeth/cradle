package models

import play.api.db._
import play.api.db.slick._
import play.api.db.slick.Config.driver.simple._
import play.api.Play.current
import scala.slick.session.Database.threadLocalSession
import scala.util.Try
import scala.util.Success
import scala.util.Failure

case class Bar(id: Option[Long] = None, name: String, address: String, verified: Boolean) {
  override def toString = "Bar - " + name
  override def equals(other: Any): Boolean = other match {
    case Bar(_, name, _, _) => this.name.equals(name)
    case _ => false
  }
}

object Bars extends CRUDModel[Bar]("BAR") {

  def id = column[Long]("ID", O.PrimaryKey, O AutoInc) // This is the primary key column
  def name = column[String]("NAME")
  def address = column[String]("ADDRESS")
  def verified = column[Boolean]("VERIFIED")

  def nameIndex = index("bar_name_idx", name, unique = true)

  def * = id.? ~ name ~ address ~ verified <> (Bar.apply _, Bar.unapply _)

  /**
   * Update a bar
   * @param id
   * @param bar
   */
  def update(id: Long, bar: Bar) {
    database.withTransaction {
      val barToUpdate: Bar = bar.copy(Some(id))
      Bars.where(_.id === id).update(barToUpdate)
    }
  }
  
  /**
   * Register a bar
   * @param id
   * @param bar
   */
  def register(bar: Bar, newHostId: Long) = database.withTransaction {
      val newBarId = Bars.insert(bar)
      Hosts.insert(Host(None, userId = newHostId, barId = newBarId, barAdmin = true))
  }

  /**
   * Display a bar with its tables
   */
  def withTables(id: Long): (Bar, Seq[BarTable]) = database.withSession {
    val bar = findByIdOrThrowIfNot(id)

    val tbs = for {
      table <- BarTables
      if (table.barId === id)
    } yield table

    (bar, tbs.list)
  }

  /**
   * Add a new table to a bar
   * @param bar - the bar of the logged in host
   * @param tableName - the name of the new table
   */
  def addTable(tableName: String)(implicit bar: Bar) = {
    val newTableId = BarTables.insert(BarTable(id = None, tableName, bar.id.get))
    newTableId
  }

  /**
   * Update the basic infos of a given table
   * @param id - id of the table
   * @param bar - the bar of the logged in host
   * @param tableName - the name of the new table
   */
  def updateTable(id: Long, tableName: String)(implicit bar: Bar) = database.withSession {
    BarTables.where(bt => (bt.id === id) && (bt.barId === bar.id)).map(_.name).update(tableName)
  }

  /**
   * Delete table
   * @param id - id of the table
   * @param bar - the bar of the logged in host
   */
  def deleteTable(id: Long)(implicit bar: Bar) = database.withSession {
    BarTables.where(bt => (bt.id === id) && (bt.barId === bar.id)).delete
  }

  /**
   * Listing the hosts of a bar
   */
  def listHosts(implicit bar: Bar) = database.withSession {
    val hostUsers = (for {
      (host, user) <- Hosts join Guests on (_.userId === _.id)
      if (host.barId === bar.id)
    } yield (host, user)).list.map { case (h, u) => HostUser(h, u) }
    hostUsers
  }

  /**
   * Listing the admins of a bar
   */
  def listAdmins(implicit bar: Bar) = {
    listHosts.filter(_.host.barAdmin)
  }

  /**
   * Adds a new host to the logged in user's bar
   * If the user with hostUserId is already a host, its host id is returned
   */
  def addHost(hostUserId: Long)(implicit bar: Bar): Long = database.withTransaction {
    Guests.findByIdOrThrowIfNot(hostUserId)
    if (Hosts.isHost(hostUserId))
      listHosts.filter(hostUser => hostUser.user.id.get == hostUserId).flatMap(_.host.id).head
    else
      Hosts.insert(Host(None, userId = hostUserId, barId = bar.id.get, barAdmin = false))
  }

  /**
   * Adds a new admin to the logged in user's bar
   * If the user with hostUserId is already an admin, its host id is returned
   */
  def addAdmin(hostUserId: Long)(implicit bar: Bar) = database.withTransaction {
    Guests.findByIdOrThrowIfNot(hostUserId)
    if (Hosts.isHost(hostUserId)){
      val hostToAdmin = Hosts.filter(h => (h.userId === hostUserId && h.barId === bar.id.get))
      hostToAdmin.map(_.barAdmin).update(true)
      hostToAdmin.map(_.id).list.head
    }else
      Hosts.insert(Host(None, userId = hostUserId, barId = bar.id.get, barAdmin = true))
  }

  /**
   * Removes a user from the logged in user's hosts
   */
  def removeHost(hostUserId: Long)(implicit bar: Bar) = Try {
    database.withSession {
      if (lastAdmin(hostUserId))
        throw new IllegalArgumentException("you cannot remove the last admin")
      else
        Hosts.filter(h => (h.userId === hostUserId && h.barId === bar.id.get)).delete
    }
  }

  /**
   * Removes a user from the logged in user's admins
   */
  def removeAdmin(hostUserId: Long)(implicit bar: Bar) = Try {
    logger.debug("remove Admin")
    database.withSession {
      if (lastAdmin(hostUserId)){
        logger.error("Trying to remove the last admin...")
        throw new IllegalArgumentException("you cannot remove the last admin")
      } else
        Hosts.filter(h => (h.userId === hostUserId && h.barId === bar.id.get)).map(_.barAdmin).update(false)
    }
  }

  private def lastAdmin(hostUserId: Long)(implicit bar: Bar) = {
    val admins = listAdmins
    logger.debug("admins {} toremove {}",admins,hostUserId)
    val last = admins.flatMap(_.user.id).filter(_!=hostUserId).length == 0
    logger.debug("Is he last admin? {}", last)
    last
  }
}

