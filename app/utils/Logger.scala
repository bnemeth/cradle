package utils

import org.slf4j.LoggerFactory

/**
 * Logging helper trait
 * @author bnemeth
 */
trait Logger {
	val logger = LoggerFactory.getLogger("cradle." + getClass())
}