package utils

import org.joda.time.DateTime
import org.joda.time.Period
import org.joda.time.format.PeriodFormatter
import org.joda.time.format.PeriodFormatterBuilder
import play.api.i18n.Messages
import play.api.i18n.Lang

object Formatter extends Logger {

  def elapsed(dateTime: DateTime)(implicit lang: Lang) = {

    def singularOrPlural(value: Int, singular: String, plural: String) = value match {
      case 1 => singular
      case _ => plural
    }

    val period = new Period(dateTime, DateTime.now)
    val formatter = new PeriodFormatterBuilder()
      .appendHours.appendSuffix(" ").appendSuffix(singularOrPlural(period.getHours, Messages("hour"), Messages("hours"))).appendSuffix(" ")
      .appendMinutes.appendSuffix(" ").appendSuffix(singularOrPlural(period.getMinutes, Messages("minute"), Messages("minutes"))).appendSuffix(" ")
      .appendSeconds.appendSuffix(" ").appendSuffix(singularOrPlural(period.getSeconds, Messages("second"), Messages("seconds"))).appendSuffix(" ")
      .printZeroNever
      .toFormatter;

    formatter.print(period)
  }

  def elapsedMillis(dateTime: DateTime)(implicit lang: Lang) = {
    val period = DateTime.now.getMillisOfDay() - dateTime.getMillisOfDay()
    period
  }
}