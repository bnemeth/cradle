package mocks

import models._

object Mocks {

  val MProducts = Map(
    "hamburger" -> Product(Some(1), "hamburger", "The next day I sat in the hall in my green tin chair, waiting to be called. Across from me sat a man who had something wrong with his nose. It was very red and very raw and very fat and long and it was growing upon itself. You could see where section had grown upon section. Something had irritated the man's nose and it had just started growing. I looked at the nose and then tried not to look. I didn't want the man to see me looking, I knew how he felt. But the man seemed very comfortable. He was fat and sat there almost asleep.", 1200, true),
    "keseru mez" -> Product(Some(2), "keserű méz", "I took the envelope home to my mother and handed it to her and walked into the bedroom. My bedroom. The best thing about the bedroom was the bed. I liked to stay in bed for hours, even during the day with the covers pulled up to my chin. It was good in there, nothing ever occurred in there, no people, nothing. My mother often found me in bed in the daytime.", 650, true),
    "csokimousse" -> Product(Some(3), "csokimousse", "I heard my father come in. He always slammed the door, walked heavily, and talked loudly. Hewas home. After a few moments the bedroom door opened. He was six feet two, a large man.Everything vanished, the chair I was sitting in, the wallpaper, the walls, all of my thoughts. He was thedark covering the sun, the violence of him made everything else utterly disappear. He was all ears,nose, mouth, I couldn't look at his eyes, there was only his red angry face.", 750, false),
    "tócsni" -> Product(Some(4), "tócsni", "leírás 4", 555, true),
    "chips" -> Product(Some(5), "chips", "leírás 4", 450, true),
    "aprohal" -> Product(Some(6), "apró hal", "leírás 4", 550, true),
    "hermelin" -> Product(Some(7), "hermelin", "leírás 4", 750, true),
    "langallo" -> Product(Some(8), "langalló", "leírás 4", 750, true),
    "quesadillas" -> Product(Some(9), "quesadillas", "leírás 4", 1250, true),
    "krokett" -> Product(Some(10), "krokett", "leírás 4", 850, true),
    "gorog_salata" -> Product(Some(11), "görög saláta", "leírás 4", 1250, true),
    "pho" -> Product(Some(12), "phở", "leírás 4", 1050, true),
    "csevap" -> Product(Some(13), "csevap", "leírás 4", 1350, true),
    "kacsaburger" -> Product(Some(14), "kacsaburger", "leírás 4", 1750, true),
    "lazac tikka" -> Product(Some(15), "lazac tikka", "leírás 4", 1750, true),
    "chillis bab" -> Product(Some(16), "chillis bab", "leírás 4", 1350, true))

  def sumOfOrder(list: List[(String, Int)]) = {
    list.foldLeft(0L)((acc, productAndCount) => { acc + MProducts.apply(productAndCount._1).price * productAndCount._2 })
  }

  val users = Map(
    "tom" -> User(Some(1), "Tamáska", "emailjeno@gmail.com", "843360392", "facebook"),
    "itu" -> User(Some(2), "Itu", "emailgeza@gmail.com", "1028666580", "facebook"),
    "atika" -> User(Some(3), "Atika", "emailgezuka@gmail.com", "564348916", "facebook"),
    "balazs" -> User(Some(4), "Balázs", "nmthblzs@gmail.com", "", "114250837373732666468"),
    "admin" -> User(Some(1), "Tamáska", "emailjeno@gmail.com", "843360392", "facebook")
    )
    
    val bars = Map (
        "mybar" -> Bar(Some(1), name = "presszó", address = "1119 Budafoki ut 100", true),    
        "yourbar" -> Bar(Some(2), name = "9. számú italbolt", address = "Acsalag", false)
    )
}