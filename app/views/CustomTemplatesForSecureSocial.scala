package views

import play.api.mvc.{RequestHeader, Request}
import play.api.templates.{Html, Txt}
import play.api.{Plugin, Application}
import securesocial.core.{Identity, SecuredRequest, SocialUser}
import play.api.data.Form
import securesocial.controllers.Registration.RegistrationInfo
import securesocial.controllers.PasswordChange.ChangeInfo
import securesocial.controllers.TemplatesPlugin
import securesocial.controllers.DefaultTemplatesPlugin
import utils.Logger
import models._
import securesocial.core.SecureSocial
import play.api.i18n.Lang

class CustomTemplatesForSecureSocial(application: Application) extends DefaultTemplatesPlugin(application) with Logger{
 /**
   * Returns the html for the login page
   * @param request
   * @tparam A
   * @return
   */
  override def getLoginPage[A](implicit request: Request[A], form: Form[(String, String)],
                               msg: Option[String] = None): Html = {
    
    implicit val lang: Lang = request.acceptLanguages match {
      case (head::tail) =>  head
      case _ => Lang.defaultLang
    }
    
    val checkinToTablePattern = "(/checkin/)(\\d+)".r
    
    val barTabletoCheckinTo : Option[BarTable]= request.session.get(SecureSocial.OriginalUrlKey) match {
      case Some(checkinToTablePattern(prefix, tableId)) => BarTables.findById(tableId.toLong)
      case _ => None
    }
    
    views.html.login.login(form, barTabletoCheckinTo)
  }
}