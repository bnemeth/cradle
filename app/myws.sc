object myws {
  val l = List("a","b","c")                       //> l  : List[String] = List(a, b, c)
  l.map(_.trim()).mkString(",")                   //> res0: String = a,b,c
  
}