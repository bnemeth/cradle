package global

import play.api.Play.current
import play.api.db.slick._
import com.google.inject.AbstractModule
import net.codingwell.scalaguice.ScalaModule
import utils._

object MyResources {

  class MyResources extends AbstractModule with ScalaModule {
    def configure {
      bind[MyDB].to[DBW]
    }
  }

  class TestResources extends AbstractModule with ScalaModule with Logger{
    def configure {
      logger.debug("binding test database")
      bind[MyDB].to[TestDB]
    }
  }

  trait MyDB {
    def database: Database
  }

  class DBW extends MyDB {
    def database = DB
  }

  class TestDB extends MyDB {
    def database = DB("testdb")
  }

}