package global

import play.api.GlobalSettings
import models._
import play.api.Application
import play.api.Play.current
import play.api.Mode._
import scala.slick.jdbc.{ StaticQuery => Q }
import org.joda.time.DateTime
import mocks.Mocks._
import play.api.db._
import play.api.Play.current
import play.api.db.slick._
import play.api.db.slick.Config.driver.simple._
import scala.slick.session.Database.threadLocalSession
import com.google.inject.Guice
import com.google.inject.AbstractModule
import net.codingwell.scalaguice.ScalaModule
import global.MyResources._
import net.codingwell.scalaguice.InjectorExtensions
import play.api.Play
import utils._

object Global extends GlobalSettings with Logger {

  lazy val injector =
    Play.mode match {
      case Prod => InjectorExtensions.enrichInjector(Guice.createInjector(new MyResources))
      case Test => InjectorExtensions.enrichInjector(Guice.createInjector(new MyResources))
      case Dev => InjectorExtensions.enrichInjector(Guice.createInjector(new MyResources))
    }

  override def onStart(app: Application) {

    def database = injector.instance[MyDB].database

    database withSession {
      // Create the tables, including primary and foreign keys
      val ddl = (Bars.ddl ++ BarTables.ddl ++ Orders.ddl ++ Products.ddl ++ Seats.ddl ++ Tags.ddl ++ Guests.ddl ++ Hosts.ddl ++ LineItems.ddl ++ ProductTags.ddl)

      val drop = "DROP ALL OBJECTS"
      Q.updateNA(drop).execute
      ddl.create

      Bars.insertAll(Seq(
              bars("mybar"),
              bars("yourbar")
      ))
      
      // Insert some suppliers
      BarTables.insertAll(
        Seq(
          BarTable(Some(1), "1-es",1),
          BarTable(Some(2), "2-es",1),
          BarTable(Some(3), "3-as",1)))

      Products.insertAll(
        Seq(
          MProducts("hamburger"),
          MProducts("keseru mez"),
          MProducts("csokimousse"),
          MProducts("tócsni"),
          MProducts("chips"),
          MProducts("aprohal"),
          MProducts("hermelin"),
          MProducts("langallo"),
          MProducts("quesadillas"),
          MProducts("krokett"),
          MProducts("gorog_salata"),
          MProducts("pho"),
          MProducts("csevap"),
          MProducts("kacsaburger"),
          MProducts("lazac tikka"),
          MProducts("chillis bab")))

      Guests.insertAll(
        Seq(
          users("tom"), users("itu"), users("atika"), users("balazs")))

      Hosts.insertAll(
        Seq(
          Host(Some(1), 1, 1),
          Host(Some(2), 4, 1, true)))

      Orders.insertAll(
        Seq(
          Order(Some(1), 1, DateTime.now, Some(DateTime.now)),
          Order(Some(2), 1, DateTime.now, Some(DateTime.now)),
          Order(Some(3), 1, DateTime.now),
          Order(Some(4), 1, DateTime.now),
          Order(Some(5), 2, DateTime.now)))

      LineItems.insertAll(
        Seq(
          LineItem(Some(1), 1, 1),
          LineItem(Some(2), 1, 1),
          LineItem(Some(3), 2, 1),
          LineItem(Some(4), 2, 3),
          LineItem(Some(5), 2, 1),
          LineItem(Some(6), 1, 2)))

      Seats.insertAll(
        Seq(
          Seat(Some(1), 1, 1),
          Seat(Some(2), 1, 2),
          Seat(Some(3), 2, 3)))

      Tags.insertAll(
        Seq(
          Tag(Some(1), "étel"),
          Tag(Some(2), "ital"),
          Tag(Some(3), "sör"),
          Tag(Some(4), "hamburger")))

      ProductTags.insertAll(
        Seq(
          ProductTag(Some(1), MProducts("hamburger").id.get, 1),
          ProductTag(Some(2), MProducts("hamburger").id.get, 4),
          ProductTag(Some(3), MProducts("keseru mez").id.get, 2),
          ProductTag(Some(4), MProducts("keseru mez").id.get, 3),
          ProductTag(Some(5), MProducts("kacsaburger").id.get, 1),
          ProductTag(Some(6), MProducts("kacsaburger").id.get, 4),
          ProductTag(Some(7), MProducts("hermelin").id.get, 1),
          ProductTag(Some(8), MProducts("csevap").id.get, 1)))

    }
  }
}