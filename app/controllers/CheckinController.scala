package controllers
import scala.slick.driver.H2Driver.simple._
import play.api._
import play.api.mvc._
import play.api.db._
import play.api.Play.current
import models._
import Database.threadLocalSession
import play.api.libs.json._
import utils.Logger
import securesocial.core._
import controllers.base.AbstractSecuredController

object CheckinController extends AbstractSecuredController {

  val PlaceOrder = Redirect(routes.OrderController.newOrder)

  def checkin(tableId: Long) = SecuredAction { implicit request =>

    logger.debug("checking to {}", tableId)

    val guestId = loggedInUser.id.get;
	BarTables.checkinUserToTable(guestId, tableId)
	Ok("checkin successful")
	PlaceOrder
  }
  
  def checkout = SecuredAction {implicit request =>
    val guestId = loggedInUser.id.get;
	BarTables.checkoutUserFromTable(guestId)
    Redirect(routes.CheckinController.listBars)
  }
  
  def listBars = SecuredAction {implicit request =>
      val bars = Bars.listAll
      Ok(views.html.order.checkin_bars(bars))
  }
  
  
  def listTables(barId: Long) = SecuredAction {implicit request =>
    val tables = BarTables.listTablesOfBar(barId)
    Ok(views.html.order.checkin_tables(tables))
  }
  
}