package controllers

import controllers.base.AbstractSecuredController
import models.Product
import models.ProductWithTags
import models.Products
import models.Tag
import play.api.data.Form
import play.api.data.Forms.checked
import play.api.data.Forms.longNumber
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText
import play.api.data.Forms.optional
import play.api.i18n.Messages
import play.api.libs.json.Json

object ProductController extends AbstractSecuredController {

  val Home = Redirect(routes.ProductController.list())

  val productForm = Form(
    mapping(
      "id" -> optional(longNumber),
      "name" -> nonEmptyText,
      "description" -> nonEmptyText,
      "price" -> longNumber,
      "availability" -> checked(""))(Product.apply)(Product.unapply))

  val productWithTagsForm = Form(
    mapping(
      "id" -> optional(longNumber),
      "name" -> nonEmptyText,
      "description" -> nonEmptyText,
      "price" -> longNumber,
      "availability" -> checked(""),
      "tags" -> nonEmptyText)(ProductWithTags.apply2)(ProductWithTags.unapply2))

  def list = HostSecuredAction { implicit request =>
    Ok(views.html.bar.products(Products.listAll))
  }

  def filterBy(tag: String) = HostSecuredAction { implicit request =>
    Ok(views.html.bar.products(Products.listByTag(Tag(None, tag))))
  }

  def showCreate = HostSecuredAction { implicit request =>
    Ok(views.html.bar.products_new(productForm))
  }

  def showEdit(id: Long) = HostSecuredAction { implicit request =>
    Products.productWithTags(id).map { productWithTags =>
      Ok(views.html.bar.products_edit(id, productWithTagsForm.fill(productWithTags)))
    }.getOrElse(NotFound)
  }

  def saveNew = HostSecuredAction {
    implicit request =>
      productForm.bindFromRequest.fold(
        formWithErrors => {
          BadRequest(views.html.bar.products_new(formWithErrors))
        },
        product => {
          Products.insert(product)
          Home.flashing("success" -> Messages("product.created", product.name))
        })
  }

  def delete(id: Long) = HostSecuredAction { implicit request =>
    if (Products.isDeletable(id)) {
      Products.delete(id)
      Ok(Json.toJson(Map("success" -> Messages("product.deleted"))))
    } else {
      Forbidden(Json.toJson(Map("error" -> Messages("product.delete.notpossible"))))
    }
  }

  def update(id: Long) = HostSecuredAction {
    implicit request =>
      productWithTagsForm.bindFromRequest.fold(
        formWithErrors => {
          BadRequest(views.html.bar.products_edit(id, formWithErrors))
        },
        productWithTags => {
          Products.updateWithTags(id, productWithTags)
          Home.flashing("success" -> Messages("product.saved", productWithTags.product.name))
        })
  }

}