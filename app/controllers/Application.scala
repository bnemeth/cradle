package controllers

import play.api._
import play.api.mvc._
import securesocial.core._
import utils.Logger

object Application extends Controller with SecureSocial with Logger{

  def index = SecuredAction { implicit request =>
    logger.debug("logged in user {}",request.user)
    Redirect(routes.CheckinController.listBars)
  }

  def javascriptRoutes = Action { implicit request =>
    import routes.javascript._
    Ok(
      Routes.javascriptRouter("jsRoutes")(
        ProductController.delete,
        TableController.checkoutAllGuests,
        TableController.updateName
        )).as("text/javascript")
  }
}