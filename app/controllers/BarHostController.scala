package controllers
import scala.slick.driver.H2Driver.simple._
import play.api._
import play.api.mvc._
import play.api.db._
import play.api.Play.current
import models._
import Database.threadLocalSession
import play.api.libs.json._
import play.api.i18n.Messages
import controllers.base.AbstractSecuredController
import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms.optional
import play.api.data.Forms.longNumber
import play.api.data.Forms.nonEmptyText
import scala.util.Success
import scala.util.Failure

/**
 * Controller responsible for managing the bar side
 */
object BarHostController extends AbstractSecuredController {

  val HostList = Redirect(routes.BarHostController.listHosts)

  def listHosts = HostSecuredAction { implicit request =>
    val hosts = Bars.listHosts
    Ok(views.html.bar.hosts(hosts))
  }

  def addHost(userId: Long) = HostSecuredAction { implicit request =>
    Bars.addHost(userId)
    HostList
  }
  
  def addNewHost(filter: String) = HostSecuredAction { implicit request =>
    val userList = if(filter.isEmpty()) Guests.listAll else Guests.listByFilter(filter.trim.toLowerCase())
    Ok(views.html.bar.add_new_host(userList))
  }

  def addAdmin(userId: Long) = HostSecuredAction { implicit request =>
    Bars.addAdmin(userId)
    HostList
  }

  def removeHost(userId: Long) = HostSecuredAction { implicit request =>
    val message = Bars.removeHost(userId) match {
      case Success(_) =>
        "success" -> Messages("bar.host.removeSuccessful")
      case Failure(t) =>
        "error" -> Messages("bar.host.removeFailed")
    }
    HostList.flashing(message)
  }

  def removeAdmin(userId: Long) = HostSecuredAction { implicit request =>
    val message = Bars.removeAdmin(userId) match {
      case Success(_) =>
        "success" -> Messages("bar.host.removeSuccessful")
      case Failure(t) =>
        "error" -> Messages("bar.host.removeFailed")
    }
    HostList.flashing(message)
  }
}