package controllers
import scala.slick.driver.H2Driver.simple._
import play.api._
import play.api.mvc._
import play.api.db._
import play.api.Play.current
import models._
import models.Formats._
import Database.threadLocalSession
import play.api.libs.json._
import play.api.i18n.Messages
import controllers.base.AbstractSecuredController
import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms.optional
import play.api.data.Forms.longNumber
import play.api.data.Forms.nonEmptyText
import play.api.libs.json.Json

/**
 * Controller responsible for managing the bar side
 */
object BarController extends AbstractSecuredController {

  val RegisterNew = Redirect(routes.ProductController.list())

  val barForm = Form(
    mapping(
      "id" -> optional(longNumber),
      "name" -> nonEmptyText,
      "address" -> nonEmptyText)(
        ((id: Option[Long], name: String, address: String) => Bar(id, name, address, false)))((bar: Bar) => Option(bar.id, bar.name, bar.address)))

  def showEditBar = HostSecuredAction { implicit request =>
    Ok(views.html.bar.bar_edit(barForm.fill(barOfLoggedInHost)))
  }

  private def barOption(implicit loggedInUser: User) = for {
    userId <- loggedInUser.id
    bar <- Hosts.getBarOptionOfHost(userId)
  } yield (bar)

  def showCreateBar = SecuredAction { implicit request =>
    barOption match {
      case Some(_) =>
        Redirect(routes.BarController.showEditBar).flashing("error" -> Messages("bar.error.already.registered"))
      case _ =>
        Ok(views.html.bar.bar_new(barForm))
    }
  }

  def barIndex = Action { implicit request =>
    Ok(views.html.bar.bar_index())
  }

  def listMyBars = HostSecuredAction { implicit request =>
    val bars = Bars.listAll
    val jsonBars = Json.toJson(bars)
    Ok(jsonBars)
  }

  def create = SecuredAction { implicit request =>
    if (barOption.isDefined) {
      Redirect(routes.BarController.showEditBar).flashing("error" -> Messages("bar.error.already.registered"))
    } else {
      barForm.bindFromRequest.fold(
        formWithErrors => {
          BadRequest(views.html.bar.bar_edit(formWithErrors))
        }, bar => {
          val registeredBarId = Bars.register(bar, loggedInUser.id.get)
          logger.debug("Bar registered successfully with id {}", registeredBarId)
          Ok(views.html.bar.bar_edit(barForm.fill(barOfLoggedInHost))).flashing("error" -> registeredBarId.toString)
        })
    }
  }

  def update = HostSecuredAction { implicit request =>
    barForm.bindFromRequest.fold(
      formWithErrors => {
        BadRequest(views.html.bar.bar_edit(formWithErrors))
      }, bar => {
        Bars.update(barOfLoggedInHost.id.get, bar)
      })
    Ok(views.html.bar.bar_edit(barForm.fill(barOfLoggedInHost)))
  }

}