package controllers

import play.api.mvc.Controller
import play.api.mvc.WebSocket
import play.api.libs.json.JsValue
import models._
import play.api.libs.concurrent.Akka
import play.api.libs.iteratee.Concurrent
import play.api.libs.iteratee.Iteratee
import play.api.libs.iteratee.Enumerator
import play.api.Play.current
import play.api.libs.json.JsString

object WebSockets extends Controller {
  
  def events = WebSocket.async[JsValue] { request =>
  	BarFeed.init
  }
}