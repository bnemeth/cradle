package controllers
import scala.slick.driver.H2Driver.simple._
import play.api._
import play.api.mvc._
import play.api.db._
import play.api.Play.current
import models._
import Database.threadLocalSession
import play.api.libs.json._
import play.api.i18n.Messages
import controllers.base.AbstractSecuredController

/**
 * Controller responsible for managing the tables by the bar side
 */
object TableController extends AbstractSecuredController {

  def ViewTable(id: Long) = Redirect(routes.TableController.view(id))

  def list = HostSecuredAction { implicit request =>
    val barTablesWithState = BarTables.listTablesWithState
    Ok(views.html.bar.tables(barTablesWithState))
  }

  def view(id: Long) = HostSecuredAction { implicit request =>
    val barTableWithFullState = BarTables.getTableStateFull(id)
    Ok(views.html.bar.table(barTableWithFullState))
  }

  def delivered(id: Long) = HostSecuredAction { implicit request =>
    BarTables.deliverOrdersOnTable(id)
    ViewTable(id).flashing("success" -> Messages("bartable.orders.delivered.success"))
  }

  def paid(id: Long) = HostSecuredAction { implicit request =>
    BarTables.payOrdersOnTable(id)
    ViewTable(id).flashing("success" -> Messages("bartable.bill.paid.success"))
  }

  def checkoutAllGuests(id: Long) = HostSecuredAction { implicit request =>
    BarTables.checkoutTable(id)
    ViewTable(id).flashing("success" -> Messages("bartable.checkout.success"))
  }

  def showEditTables = HostSecuredAction { implicit request =>
    val barTablesWithState = BarTables.listTablesWithState
    Ok(views.html.bar.tables_edit(barTablesWithState))
  }

  def addNewTable = HostSecuredAction { implicit request =>
    Bars.addTable("new table")
    Redirect(routes.TableController.showEditTables)
  }

  def deleteTable(id: Long) = HostSecuredAction { implicit request =>
    Bars.deleteTable(id)
    Redirect(routes.TableController.showEditTables)
  }

  def updateName(id: Long, name: String) = HostSecuredAction { implicit request =>
    Bars.updateTable(id, name)
    Redirect(routes.TableController.showEditTables)
  }

  def printQRCode(id: Long) = HostSecuredAction { implicit request =>
    val barTableWithFullState = BarTables.getTableStateFull(id)
    Ok(views.html.bar.table_print_qr_code(barTableWithFullState))
  }
}