package controllers

import models._
import play.api.i18n.Messages
import controllers.base.AbstractSecuredController

object OrderController extends AbstractSecuredController {

  val ActiveOrders = Redirect(routes.OrderController.activeOrders)
  val DeliveredOrders = Redirect(routes.OrderController.deliveredOrders)

  /**
   * View the product list where the guest is able to place an order
   */
  def newOrder = SeatedAndSecuredAction { implicit request =>
	  implicit val tableWithState = BarTables.getCurrentTableState(loggedInUser.id.get)
	  Ok(views.html.order.new_order(Products.listAll))
  }
  
  /**
   * Filter the menu by the given tag
   */
  def filterMenuBy(tag: String) = SeatedAndSecuredAction { implicit request =>
    implicit val tableWithState = BarTables.getCurrentTableState(loggedInUser.id.get)
    Ok(views.html.order.new_order(Products.listByTag(Tag(None,tag))))
  }

  /**
   * Place an order here
   */
  def placeOrder = SeatedAndSecuredAction { implicit request =>
      val orderMap = request.body.asFormUrlEncoded.get
      val basket = BasketConverter.convertRequestMapToBasket(orderMap)
      Orders.placeOrder(loggedInUser.id.get, basket)
      logger.debug("Basket: {}", basket)
      ActiveOrders.flashing("success" -> Messages("order.placed"))
  }

  /**
   * List the active orders
   */
  def activeOrders = SeatedAndSecuredAction { implicit request =>
      implicit val tableWithState = BarTables.getCurrentTableState(loggedInUser.id.get)
      val activeOrders = tableWithState.ordersGroupedByStateWithOrdersSum.get(Active)
      Ok(views.html.order.active_orders(activeOrders))
  }

  /**
   * List the delivered (unpaid) orders
   */
  def deliveredOrders = SeatedAndSecuredAction { implicit request =>
      implicit val tableWithState = BarTables.getCurrentTableState(loggedInUser.id.get)
      val deliveredOrders = tableWithState.ordersGroupedByStateWithOrdersSum.get(Delivered)
      Ok(views.html.order.delivered_orders(deliveredOrders))
  }
  
  /**
   * The logged in guest wants to pay
   */
  def pay = SeatedAndSecuredAction { implicit request =>
    BarTables.guestWantsToPay(loggedInUser.id.get)
    DeliveredOrders.flashing("success" -> Messages("pay.request.sent"))
  }
}

object BasketConverter {

  val productIdInputPattern = "(product_)(.*)".r

  def convertRequestMapToBasket(orderMap: Map[String, Seq[String]]) = Basket(orderMap.filter(productWithOrderedCount => filterEmpty(productWithOrderedCount._2)).map { productWithOrderedCount =>
    productIdStringToLong(productWithOrderedCount._1) -> productWithOrderedCount._2.head.toLong
  })

  private def filterEmpty(count: Seq[String]) = {
    !count.head.isEmpty()
  }

  private def productIdStringToLong(productIdString: String) = productIdString match {
    case productIdInputPattern(prefix, productId) => productId.toLong
    case _ => -1L
  }
}