package controllers.base

import scala.slick.driver.H2Driver.simple._
import play.api._
import play.api.mvc._
import play.api.db._
import models._
import play.api.libs.json._
import utils.Logger
import securesocial.core._
import controllers.routes

trait AbstractSecuredController extends Controller with SecureSocial with Logger {

  val ListBars = Redirect(routes.CheckinController.listBars)

  implicit def loggedInUser(implicit request: SecuredRequest[AnyContent]) = request.user match {
    case SocialWrappedUser(user) => user
  }
  
  implicit def barOfLoggedInHost(implicit request: SecuredRequest[AnyContent]): Bar = 
    Hosts.getBarOfHost(loggedInUser.id.get) 

  /**
   * Checks if the guest already checked in into a table
   * If false, redirect him to the list of table for checkin
   */
  def SeatedAndSecuredAction(f: SecuredRequest[AnyContent] => Result): Action[AnyContent] = {
    SecuredAction { request =>
      if (Seats.hasGuestSeat(loggedInUser(request).id.get)) {
        f(request)
      } else {
        ListBars
      }
    }
  }

  /**
   * Checks if the user (is a host) has the rights to do a bar task
   */
  def HostSecuredAction(f: SecuredRequest[AnyContent] => Result): Action[AnyContent] = {
    SecuredAction { request =>
      if (Hosts.isHost(loggedInUser(request).id.get)) {
        f(request)
      } else {
        Unauthorized("You are not authorized to view this page")
      }
    }
  }
}