package service

import play.api.{Application}
import securesocial.core._
import securesocial.core.providers.Token
import securesocial.core.IdentityId
import utils.Logger
import models._
import mocks.Mocks._

/**
 * User service for Secure Social
 * Reading and storing users
 * MOCKING DB
 */
class UserLoginServiceMock(application: Application) extends UserServicePlugin(application) with Logger{

  def find(id: IdentityId): Option[Identity] = {
    logger.warn("MOCK DB LOGIN RUNNING")
    Some(SocialWrappedUser(users("admin")))
  }

  def findByEmailAndProvider(email: String, providerId: String): Option[Identity] = {
    logger.warn("MOCK DB LOGIN RUNNING")
    None
  }

  def save(user: Identity): Identity = {
    logger.warn("MOCK DB LOGIN RUNNING")
    SocialWrappedUser(users("admin"))
  }

  def save(token: Token) {
    logger.trace("unimplemented save token method")
  }

  def findToken(token: String): Option[Token] = {
    logger.trace("unimplemented find token method")
    None
  }

  def deleteToken(uuid: String) {
    logger.trace("unimplemented delete token method")
  }


  def deleteExpiredTokens() {
    logger.trace("unimplemented deleteExpiredTokens method")
  }
}
