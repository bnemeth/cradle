/**
 * Copyright 2012 Jorge Aliss (jaliss at gmail dot com) - twitter: @jaliss
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package service

import play.api.{Application}
import securesocial.core._
import securesocial.core.providers.Token
import securesocial.core.IdentityId
import utils.Logger
import models._

/**
 * User service for Secure Social
 * Reading and storing users
 */
class UserLoginService(application: Application) extends UserServicePlugin(application) with Logger{

  def find(id: IdentityId): Option[Identity] = {
    logger.debug("find by indentity id {}", id)
    val guest = Guests.findBySocialId(id)
    guest match {
      case Some(guest) => Some(SocialWrappedUser(guest))
      case None => None
    }
  }

  def findByEmailAndProvider(email: String, providerId: String): Option[Identity] = {
    logger.debug("findByEmailAndProvider")
    None
  }

  def save(user: Identity): Identity = {

    logger.debug("save identity: {}", user)
    
    val guest = Guests.findByEmail(user.email.get)
    val savedGuest = guest match {
      case Some(guest) => Guests.updateSocial(guest.id.get, user.identityId)
      case None => {
    	  
    	  val fbOrGoogle = user.identityId.providerId match {
    	    case "facebook" => (user.identityId.userId,"")
    	    case "google" => ("",user.identityId.userId)
    	  }
    	  val newGuest = User(None, user.fullName, 
    			  user.email.get, 
    			  fbOrGoogle._1, 
    			  fbOrGoogle._2, 
    			  user.avatarUrl)
    			  val newId = Guests.insert(newGuest)
    			  val savedGuest = User(Some(newId), user.fullName, user.email.get, fbOrGoogle._1, fbOrGoogle._2, user.avatarUrl)
    			  savedGuest
      }
    }
    logger.debug("saved and returned {} ", SocialWrappedUser(savedGuest))
    SocialWrappedUser(savedGuest)
  }

  def save(token: Token) {
    logger.trace("unimplemented save token method")
  }

  def findToken(token: String): Option[Token] = {
    logger.trace("unimplemented find token method")
    None
  }

  def deleteToken(uuid: String) {
    logger.trace("unimplemented delete token method")
  }


  def deleteExpiredTokens() {
    logger.trace("unimplemented deleteExpiredTokens method")
  }
}
