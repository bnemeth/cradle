import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName = "cradle"
  val appVersion = "0.1"

  val slickVersion = "1.0.1"
  //      val slickVersion = "2.0.0-M3"

  //  lazy val playSlickSNAP = RootProject(uri("git://github.com/prakhunov/play-slick.git"))

  val appDependencies = Seq(
    jdbc,
    anorm,
    "com.typesafe.slick" % "slick_2.10" % slickVersion,
    "com.typesafe.play" %% "play-slick" % "0.5.0.8",
    "net.codingwell" % "scala-guice_2.10" % "4.0.0-beta",
    "log4j" % "log4j" % "1.2.16",
    "com.h2database" % "h2" % "1.3.166",
    "org.seleniumhq.selenium" % "selenium-java" % "2.31.0",
    "securesocial" %% "securesocial" % "master-SNAPSHOT")

  val main = play.Project(appName, appVersion, appDependencies).settings(defaultScalaSettings: _*).settings(
    resolvers += "Sonatype OSS Releases" at "http://oss.sonatype.org/content/repositories/releases/",
    resolvers += "Sonatype OSS Snapshots" at "http://oss.sonatype.org/content/repositories/snapshots/",
    resolvers += "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/",
    //    resolvers += "sbt-plugin-snapshots"  at "http://repo.scala-sbt.org/scalasbt/sbt-plugin-snapshots/",
    resolvers += Resolver.url("sbt-plugin-snapshots", new URL("http://repo.scala-sbt.org/scalasbt/sbt-plugin-snapshots/"))(Resolver.ivyStylePatterns),
    scalacOptions ++= Seq("-feature", "-language:postfixOps"))
  //dependsOn (playSlickSNAP)
}
