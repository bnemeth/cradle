$(document).ready(function(){

	//Products delete button
	$('.deleteProduct').click(function(e){
		e.preventDefault();
		var id = $(this).attr('data-id');
		jsRoutes.controllers.ProductController.delete(id).ajax({
	    	success: function(result) {
	    		$('.info_messages').remove();
	    		$('.error_messages').remove();

	    		$('.productRow'+id).remove();
	    	    var result = JSON.parse(JSON.stringify(result));
	    	    $('#footer').append('<div class="info_messages closable">' + result.success + '</div>');
	    	},
	    	error : function(result) {
	    	    $('.info_messages').remove();
	    	    $('.error_messages').remove();

	    	    var result = JSON.parse(result.responseText);
	    		$('#footer').append('<div class="error_messages closable">' + result.error + '</div>');
	    	}
		});
	});
	
	//Checkout all guests from a table
	$('.checkoutAllGuests').click(function(e){
		e.preventDefault();
		$('form#checkoutAllGuests').submit();
	});

	//Set delivered orders paid	
	$('.deliveredOrdersPaid').click(function(e){
		e.preventDefault();
		$('form#deliveredOrdersPaid').submit();
	});
	
	//Set active orders delivered	
	$('.activeOrdersDelivered').click(function(e){
		e.preventDefault();
		$('form#activeOrdersDelivered').submit();
	});
	
	//Save product	
	$('.saveProduct').click(function(e){
		e.preventDefault();
		$('form#editProduct').submit();
	});
	//Save bar data
	$('.saveBar').click(function(e){
		e.preventDefault();
		$('form#editBar').submit();
	});	
	
	//Save new bar data
	$('.createBar').click(function(e){
		e.preventDefault();
		$('form#createBar').submit();
	});
	
	//Bind numeric only
   	$('form#editProduct #price').numeric({ negative: false }, function() { alert("Positive integers only"); this.value = ""; this.focus(); });
   	
   	$.timeelapse.settings.patterns = {          
   			milliseconds : '%d',
   			seconds: '%d másodperc',
   			minutes: '%d perc',
   			hours: '%d óra',
   			days: '%d',
   			years: '%d',
   			separator: ' '
   	}; // Localization settings.
   	
   	$('.elapsed').each(function(){
   		var elapsed = $(this).attr('data-elapsed');
   		$.timeelapse(elapsed,$(this));
   	});
   	
   	//Update table name
	$('.updateTableName').click(function(e){
		e.preventDefault();
		var id = $(this).attr('data-id');
		var name = $('input.tableName' + id)
		jsRoutes.controllers.TableController.updateName(id,name.val()).ajax({
	    	success: function(result) {
	    		$('.info_messages').remove();
	    		$('.error_messages').remove();

	    	    var result = JSON.parse(JSON.stringify(result));
	    	    $('#footer').append('<div class="info_messages closable">' + result.success + '</div>');
	    	},
	    	error : function(result) {
	    	    $('.info_messages').remove();
	    	    $('.error_messages').remove();

	    	    var result = JSON.parse(result.responseText);
	    		$('#footer').append('<div class="error_messages closable">' + result.error + '</div>');
	    	}
		});
	});
	
	//Add new table
	$('.addNewTable').click(function(e){
		e.preventDefault();
		$('form#addNewTable').submit();
	});
	
	//Delete table	
	$('.deleteTable').click(function(e){
		e.preventDefault();
		var id = $(this).attr('data-id');
		$('form#deleteTable' + id).submit();
	});
	
	//Bar host management
	$('.addAdmin').click(function(e){
		e.preventDefault();
		var id = $(this).attr('data-id');
		$('form#addAdmin' + id).submit();
	});
	
	$('.addHost').click(function(e){
		e.preventDefault();
		var id = $(this).attr('data-id');
		$('form#addHost' + id).submit();
	});
	
	$('.removeHost').click(function(e){
		e.preventDefault();
		var id = $(this).attr('data-id');
		$('form#removeHost' + id).submit();
	});	
	
	$('.removeAdmin').click(function(e){
		e.preventDefault();
		var id = $(this).attr('data-id');
		$('form#removeAdmin' + id).submit();
	});
});