$(document).ready(function(){

	//Checkin to table	
//	$('.checkin').click(function(e){
//		e.preventDefault();
//		$('form#checkin').submit();
//	});
	
	//Check out from table	
	$('.checkout').click(function(e){
		e.preventDefault();
		$('form#checkout').submit();
	});
	
	//Add product to order
    $('.addProductToOrder').click(function(e){
    	e.preventDefault();
    	var id = $(this).attr('data-id');
    	$('#product_'+id).val(parseInt($('#product_'+id).val()) + 1);
    	$('#product_count_'+id).text(parseInt($('#product_'+id).val()));
    	$('#orders_total span').text(parseInt($('#product_price_'+ id +' span').text()) + parseInt($('#orders_total span').text()))
    });
    
    //Remove product from order
    $('.removeProductFromOrder').click(function(e){
    	e.preventDefault();
    	var id = $(this).attr('data-id');
    	var count = parseInt($('#product_'+id).val()) - 1;
    	if(count > -1){
	    	$('#product_'+id).val(count);
    		$('#product_count_'+id).text(count);
    		$('#orders_total span').text(-parseInt($('#product_price_'+ id +' span').text()) + parseInt($('#orders_total span').text()))
    	}
    });
    
    //Place an order
	$('.placeOrder').click(function(e){
		e.preventDefault();
		$('form#placeOrder').submit();
	});
	
    //Pay
	$('.pay').click(function(e){
		e.preventDefault();
		$('form#pay').submit();
	});
	
	//Show-hide description
	$('.product_name').click(function(){
		var id = $(this).attr('data-id');
		var desc = $(this).attr('data-desc');
		var name = $(this).attr('data-name');
		$('#product_description h4').text(name);
		$('#product_description span').text(desc);
		$('#order_form').hide();
		$('#product_description').show();
	})
	$('#backToMenu').click(function(){	
		$('#product_description').hide();
		$('#order_form').show();
	})
	$('.closable').append('<a href="#" class="close">×</a>');
	$('.close').click(function(e){
		e.preventDefault();
		$(this).parent().hide();
	});
   	
   	$('.elapsed').each(function(){
   		var elapsed = $(this).attr('data-elapsed');
   		$.timeelapse(elapsed,$(this));
   	});
});