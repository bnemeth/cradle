function getLocation() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPosition);
	} else {
		console.log("Geolocation is not supported by this browser.");
	}
}
function showPosition(position) {
	var location = new google.maps.LatLng(position.coords.latitude,
			position.coords.longitude);
	var request = {
		"location" : location
	}
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode(request, showByPoint);
}

function showAddress(address) {
	var geocoder = new google.maps.Geocoder();
	var request = {
		"address" : address
	}
	geocoder.geocode(request, showByPoint);
}
function showByPoint(point) {
	if (!point || !point[0]) {
		// console.log(address + " not found");
	} else {
		if ($("#address").val() == '') {
			$("#address").val(point[0].formatted_address);
		}
		myLatlng = point[0].geometry.location;
		var mapOptions = {
			zoom : 13,
			center : myLatlng
		};
		var map = new google.maps.Map(document.getElementById('map-canvas'),
				mapOptions);
		// To add the marker to the map, use the 'map' property
		var marker = new google.maps.Marker({
			position : myLatlng,
			map : map,
			title : "Click and Drink"
		});
	}
}
var delay = (function() {
	var timer = 0;
	return function(callback, ms) {
		clearTimeout(timer);
		timer = setTimeout(callback, ms);
	};
})();

$(document).ready(function() {
	if ($("#address").val() == '') {
		getLocation();
	} else
		showAddress($("#address").val());
	$("#address").keyup(function() {
		delay(function() {
			showAddress($("#address").val());
		}, 700);
	});
});